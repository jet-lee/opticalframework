package de.virality.opticalframework.samples.matching;

import de.virality.opticalframework.core.cluster.Cluster;
import de.virality.opticalframework.core.cluster.KMeansPlusPlusClustering;
import de.virality.opticalframework.core.feature.Surf;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.gui.ImageVisualization;
import de.virality.opticalframework.core.util.ImageUtils;
import de.virality.opticalframework.matching.bow.BagOfWords;
import de.virality.opticalframework.matching.bow.Vocabulary;
import de.virality.opticalframework.matching.database.DatabaseImage;
import de.virality.opticalframework.matching.database.SurfBagOfWordsDatabase;
import de.virality.opticalframework.matching.matcher.BagOfWordsMatcher;
import de.virality.opticalframework.matching.matcher.ImageMatch;
import de.virality.opticalframework.matching.matcher.ImageMatcher;
import de.virality.opticalframework.matching.util.DbUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SurfWordLogoMatcher {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary(Surf.NATIVE_LIBRARY_NAME);
    }

    private static final String IMAGE_SET_NAME = "app_logos";

    private static final String VOCABULARY_NAME = "app_logos_kmeans++_128";

    public static void main(String[] args) {
        String url = "jdbc:postgresql://127.0.0.1:5432/optSampleSurfWordLogoMatcher";
        String username = "postgres";
        String password = "";

        SurfBagOfWordsDatabase database = new SurfBagOfWordsDatabase(url, username, password);
        Surf surf = new Surf();

        /* Clear the database if required */
        //clearDatabase(url, username, password);

        /* Setup the database if required */
        //setUpDatabase(database, surf);

        ImageMatcher matcher = new BagOfWordsMatcher<>(database, IMAGE_SET_NAME, VOCABULARY_NAME, surf);

        Mat query = ImageUtils.loadImageFromPath("assets/query_images/1.png");
        List<ImageMatch> matches = matcher.matchImageKnn(query, 4);

        if (matches.isEmpty()) {
            System.out.println("No match found!");
        } else {
            new ImageVisualization(query).show("Query");
            for (ImageMatch match : matches) {
                new ImageVisualization(match.image.path).show("Match # score = " + match.score + match.image.path);
            }
        }
    }

    private static void setUpDatabase(SurfBagOfWordsDatabase database, Surf surf) {
        System.out.println("Create image set...");
        database.createImageSet("assets/app_logos", IMAGE_SET_NAME);
        List<DatabaseImage> images = database.getImagesForImageSet(IMAGE_SET_NAME);

        System.out.println("Save SURF points...");
        for (DatabaseImage img : images) {
            Mat mat = ImageUtils.loadImageFromPath(img.path);
            List<SurfFeaturePoint> points = surf.extractFeaturePoints(mat);
            database.saveFeaturePointsForImage(img.id, points);
        }

        System.out.println("Load SURF points...");
        List<SurfFeaturePoint> points = database.loadRandomFeaturePointsSampleForImageSet(IMAGE_SET_NAME, 0.2f);
        System.out.println("#points: " + points.size());

        System.out.println("Create vocabulary...");
        KMeansPlusPlusClustering kMeans = new KMeansPlusPlusClustering(128, -1);
        List<Cluster> clusters = kMeans.calculateClusters(points);
        database.createVocabulary(VOCABULARY_NAME, clusters);
        System.out.println("clusters = " + clusters);

        System.out.println("Save bag of words for image set...");
        Vocabulary vocabulary = database.loadVocabulary(VOCABULARY_NAME);
        for (DatabaseImage img : images) {
            List<SurfFeaturePoint> pointList = database.loadFeaturePointsForImage(img.id);
            BagOfWords bagOfWords = vocabulary.calculateBagOfWords(pointList);
            database.saveBagOfWordsForImage(img.id, bagOfWords);
            System.out.println("bagOfWords = " + bagOfWords);
        }
    }

    private static void clearDatabase(String url, String username, String password) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, username, password);
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(
                    "DROP TABLE IF EXISTS imagesets, images, surfpoints, vocabularies, clusters, pointword, bows");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeSilently(connection);
        }
    }
}
