package de.virality.opticalframework.samples.odometry;

import de.virality.opticalframework.core.feature.Surf;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.gui.ImageVisualization;
import de.virality.opticalframework.core.gui.drawer.FeaturePointMatchDrawer;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.util.CameraUtils;
import de.virality.opticalframework.core.util.VideoUtils;
import de.virality.opticalframework.odometry.compass.SurfMovementCompass;
import de.virality.opticalframework.odometry.gui.OdometryDrawer;
import de.virality.opticalframework.odometry.outlier.OutlierDetection;
import de.virality.opticalframework.odometry.outlier.SimpleMeanOutlierDetection;
import de.virality.opticalframework.odometry.pedometer.SurfOrientationPedometer;
import de.virality.opticalframework.odometry.video.ParallelVideoProcessor;
import org.opencv.core.Core;
import org.opencv.highgui.VideoCapture;

import javax.swing.*;
import java.util.List;

public class SurfOdometry {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary(Surf.NATIVE_LIBRARY_NAME);
    }

    private final SurfOrientationPedometer pedometer;
    private final SurfMovementCompass compass;

    private final ParallelVideoProcessor videoProcessor;

    private ImageVisualization visualization;
    private FeaturePointMatchDrawer<SurfFeaturePoint> matchDrawer = new FeaturePointMatchDrawer<>();
    private OdometryDrawer odometryDrawer = new OdometryDrawer();

    private SurfOdometry() {
        final VideoCapture capture = new VideoCapture(SurfOdometry.class.getResource("/50_steps.m4v").getPath());
        final Surf surf = new Surf();
        final OutlierDetection outlierDetection = new SimpleMeanOutlierDetection(4);
        this.videoProcessor = new ParallelVideoProcessor(capture, surf, outlierDetection);

        this.pedometer = SurfOrientationPedometer.createDefaultFor(capture);
        this.compass = new SurfMovementCompass(VideoUtils.getHorizontalResolution(capture),
                CameraUtils.GoProConfig.N16x9.horizontalFov);
    }

    private void start() {
        videoProcessor.startProcessing();

        while (videoProcessor.next()) {
            List<FeaturePointMatch<SurfFeaturePoint>> matches = videoProcessor.getFramePointMatches();

            pedometer.feed(matches);
            compass.feed(matches);

            updateVideo();
        }
    }

    private void updateVideo() {
        if (visualization == null) {
            visualization = new ImageVisualization(videoProcessor.getFrameImage());
            visualization.setCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            visualization.addDrawer(matchDrawer);
            visualization.addDrawer(odometryDrawer);
            visualization.show();
        } else {
            odometryDrawer.setStepCount(pedometer.getStepCount());
            odometryDrawer.setHeading((int) compass.getHeading());
            matchDrawer.setMatches(videoProcessor.getFramePointMatches());
            visualization.setImage(videoProcessor.getFrameImage());
        }
    }

    public static void main(String[] args) {
        new SurfOdometry().start();
    }
}
