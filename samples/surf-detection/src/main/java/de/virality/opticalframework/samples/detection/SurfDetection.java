package de.virality.opticalframework.samples.detection;

import de.virality.opticalframework.core.feature.Surf;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.gui.drawer.FeaturePointDrawer;
import de.virality.opticalframework.core.gui.ImageVisualization;
import de.virality.opticalframework.core.util.ImageUtils;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import java.util.List;

public class SurfDetection {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary(Surf.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {
        Mat img = ImageUtils.loadImageFromPath(SurfDetection.class.getResource("/whitehouse.jpg").getPath());
        Surf surf = new Surf();

        surf.setUpright(true);
        List<SurfFeaturePoint> uprightPoints = surf.extractFeaturePoints(img);

        surf.setUpright(false);
        List<SurfFeaturePoint> orientedPoints = surf.extractFeaturePoints(img);

        ImageVisualization visualization1 = new ImageVisualization(img);
        visualization1.addDrawer(new FeaturePointDrawer<>(uprightPoints));
        visualization1.show("Default visualization");

        ImageVisualization visualization2 = new ImageVisualization(img);
        visualization2.addDrawer(new FeaturePointDrawer<>(uprightPoints, FeaturePointDrawer.Style.SURF));
        visualization2.show("SURF visualization (upright)");

        ImageVisualization visualization3 = new ImageVisualization(img);
        visualization3.addDrawer(new FeaturePointDrawer<>(orientedPoints, FeaturePointDrawer.Style.SURF));
        visualization3.show("SURF visualization (with orientation)");
    }
}
