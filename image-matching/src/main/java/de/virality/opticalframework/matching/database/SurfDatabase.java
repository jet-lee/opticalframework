package de.virality.opticalframework.matching.database;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.matching.util.DbUtils;
import de.virality.opticalframework.core.util.FileUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class SurfDatabase implements FeaturePointDatabase<SurfFeaturePoint> {

    private final String url;

    private final String username;

    private final String password;

    protected Connection connection;

    protected boolean autoClose;

    public SurfDatabase(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    protected void setAutoClose(boolean autoClose) {
        this.autoClose = autoClose;
    }

    @Override
    public void createImageSet(String path, String imageSetName) {
        try {
            ensureConnected();
            ensureTablesCreated();

            int imageSetId = writeImageSet(imageSetName);
            writeImages(path, imageSetId);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeIfNeeded();
        }
    }

    @Override
    public List<DatabaseImage> getImagesForImageSet(String imageSetName) {
        List<DatabaseImage> images = new ArrayList<>();

        String sql  =
                "SELECT images.id, images.path " +
                "FROM images JOIN imagesets ON images.imageset = imagesets.id " +
                "WHERE imagesets.name = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            statement = connection.prepareStatement(sql);
            statement.setString(1, imageSetName);

            result = statement.executeQuery();
            while (result.next()) {
                int imageId = result.getInt("id");
                String imagePath = result.getString("path");
                images.add(new DatabaseImage(imageId, imagePath));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }

        return images;
    }

    @Override
    public void saveFeaturePointsForImage(int imageId, List<SurfFeaturePoint> featurePoints) {
        PreparedStatement updatePointCountStatement = null;
        PreparedStatement insertSurfPointStatement = null;
        try {
            ensureConnected();

            String sql = "UPDATE images SET surfpointscount = ? WHERE id = ?";
            updatePointCountStatement = connection.prepareStatement(sql);

            sql = "INSERT INTO surfpoints(image, x, y, scale, orientation, descriptor) VALUES(?, ?, ?, ?, ?, ?)";
            insertSurfPointStatement = connection.prepareStatement(sql);

            updatePointCountStatement.setInt(1, featurePoints.size());
            updatePointCountStatement.setInt(2, imageId);
            updatePointCountStatement.executeUpdate();

            for (SurfFeaturePoint p : featurePoints) {
                insertSurfPointStatement.setInt(1, imageId);
                insertSurfPointStatement.setInt(2, (int) p.getX());
                insertSurfPointStatement.setInt(3, (int) p.getY());
                insertSurfPointStatement.setDouble(4, p.getScale());
                insertSurfPointStatement.setDouble(5, p.getOrientation());

                Array descriptorArray = connection.createArrayOf("float", ArrayUtils.toObject(p.getDescriptor()));
                insertSurfPointStatement.setArray(6, descriptorArray);

                insertSurfPointStatement.addBatch();
            }

            insertSurfPointStatement.executeBatch();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(insertSurfPointStatement);
            DbUtils.closeSilently(updatePointCountStatement);
            closeIfNeeded();
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<SurfFeaturePoint> loadFeaturePointsForImage(int imageId) {
        List<SurfFeaturePoint> points = new ArrayList<>();

        String sql = "SELECT x, y, scale, orientation, descriptor FROM surfpoints WHERE image = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            statement = connection.prepareStatement(sql);
            statement.setInt(1, imageId);

            result = statement.executeQuery();
            while (result.next()) {
                points.add(getFeaturePointForRow(result));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }

        return points;
    }

    @Override
    public List<SurfFeaturePoint> loadFeaturePointsForImageSet(String imageSetName) {
        List<SurfFeaturePoint> points = new ArrayList<>();

        String sql =
                "SELECT surfpoints.x, surfpoints.y, surfpoints.scale, surfpoints.orientation, surfpoints.descriptor " +
                "FROM surfpoints JOIN images ON surfpoints.image = images.id " +
                "WHERE images.imageset = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            int imageSetId = getImageSetId(imageSetName);

            connection.setAutoCommit(false);
            statement = connection.prepareStatement(sql);
            statement.setFetchSize(50);
            statement.setInt(1, imageSetId);

            result = statement.executeQuery();
            while (result.next()) {
                points.add(getFeaturePointForRow(result));
            }

            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }

        return points;
    }

    @Override
    public List<SurfFeaturePoint> loadRandomFeaturePointsSampleForImageSet(String imageSetName, float percentage) {
        List<SurfFeaturePoint> points = new ArrayList<>();

        String sql =
                "SELECT surfpoints.x, surfpoints.y, surfpoints.scale, surfpoints.orientation, surfpoints.descriptor " +
                "FROM surfpoints JOIN images ON surfpoints.image = images.id " +
                "WHERE images.imageset = ? " +
                "ORDER BY RANDOM() LIMIT ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            int imageSetId = getImageSetId(imageSetName);
            long pointsCount = getFeaturePointCountForImageSet(imageSetId);
            int limit = (int) (pointsCount * percentage);

            connection.setAutoCommit(false);
            statement = connection.prepareStatement(sql);
            statement.setFetchSize(50);
            statement.setInt(1, imageSetId);
            statement.setInt(2, limit);

            result = statement.executeQuery();
            while (result.next()) {
                points.add(getFeaturePointForRow(result));
            }

            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }

        return points;
    }

    private int writeImageSet(String imageSetName) throws SQLException {
        String sql = "INSERT INTO imagesets(name) VALUES (?) RETURNING id";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, imageSetName);
            ResultSet result = statement.executeQuery();

            result.next();
            return result.getInt("id");
        } finally {
            DbUtils.closeSilently(statement);
        }
    }

    private void writeImages(String path, int imageSetId) throws SQLException {
        List<String> imageFiles = FileUtils.readImagePath(path);

        String sql = "INSERT INTO images(path, surfpointscount, imageset) VALUES(?, ?, ?) ";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);

            for (String img : imageFiles) {
                statement.setString(1, img);
                statement.setInt(2, 0);
                statement.setInt(3, imageSetId);
                statement.addBatch();
            }

            statement.executeBatch();
        } finally {
            DbUtils.closeSilently(statement);
        }
    }

    protected void ensureConnected() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(url, username, password);
        }
    }

    protected void ensureTablesCreated() throws SQLException {
        ensureImageSetsTableCreated();
        ensureImagesTableCreated();
        ensureSurfPointsTableCreated();
    }

    private void ensureImageSetsTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS imagesets ("
                + "id SERIAL PRIMARY KEY,"
                + "name VARCHAR(100) UNIQUE)";

        connection.createStatement().executeUpdate(sql);
    }

    private void ensureImagesTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS images ("
                + "id SERIAL PRIMARY KEY,"
                + "path VARCHAR(200),"
                + "surfpointscount INTEGER,"
                + "imageset INTEGER REFERENCES imagesets(id) ON DELETE CASCADE)";

        connection.createStatement().executeUpdate(sql);
    }

    private void ensureSurfPointsTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS surfpoints ("
                + "id SERIAL PRIMARY KEY,"
                + "image INTEGER REFERENCES images(id) ON DELETE CASCADE,"
                + "x INTEGER,"
                + "y INTEGER,"
                + "scale FLOAT,"
                + "orientation FLOAT,"
                + "descriptor FLOAT[])";

        connection.createStatement().executeUpdate(sql);
    }

    protected SurfFeaturePoint getFeaturePointForRow(ResultSet row) throws SQLException {
        int x = row.getInt("x");
        int y = row.getInt("y");
        float scale  = row.getFloat("scale");
        float orientation  = row.getFloat("orientation");

        Double[] array = (Double[]) row.getArray("descriptor").getArray();
        double[] descriptor = ArrayUtils.toPrimitive(array);

        return new SurfFeaturePoint(x, y, scale, orientation, 0, descriptor);
    }

    protected int getImageSetId(String imageSetName) throws SQLException {
        String sql = "SELECT id FROM imagesets WHERE name = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, imageSetName);

            result = statement.executeQuery();
            if (!result.next()) {
                throw new NoSuchElementException("No imageset found with name " + imageSetName);
            } else {
                return result.getInt("id");
            }
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
        }
    }

    private long getFeaturePointCountForImageSet(int imageSetId) throws SQLException {
        String sql =
                "SELECT count(*) " +
                "FROM surfpoints JOIN images ON surfpoints.image = images.id " +
                "WHERE images.imageset = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, imageSetId);

            result = statement.executeQuery();
            result.next();
            return result.getInt(1);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
        }
    }

    protected void closeIfNeeded() {
        if (autoClose) {
            DbUtils.closeSilently(connection);
        }
    }
}
