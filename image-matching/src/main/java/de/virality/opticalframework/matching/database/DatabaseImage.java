package de.virality.opticalframework.matching.database;

public class DatabaseImage {

    public int id;

    public String path;

    public DatabaseImage(int id, String path) {
        this.id = id;
        this.path = path;
    }
}
