package de.virality.opticalframework.matching.database;

import de.virality.opticalframework.core.feature.FeaturePoint;

import java.util.List;

public interface RerankingDatabase<T extends FeaturePoint> extends BagOfWordsDatabase<T> {

    void createPointWordMappingForImageSet(String imageSetName, String vocabularyName);

    List<Integer> initSession(String vocabularyName, double q);

    List<T> getRerankingFeaturePointsForImage(int imgId);

    void endSession();

}
