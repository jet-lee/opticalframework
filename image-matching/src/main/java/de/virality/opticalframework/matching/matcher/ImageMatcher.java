package de.virality.opticalframework.matching.matcher;

import org.opencv.core.Mat;

import java.util.List;

public interface ImageMatcher {

    ImageMatch matchImage(Mat image);

    List<ImageMatch> matchImageKnn(Mat image, int k);
}
