package de.virality.opticalframework.matching.bow;

import de.virality.opticalframework.core.cluster.Cluster;
import de.virality.opticalframework.core.util.ArrayUtils;

import java.util.Arrays;

public class BagOfWords {

    private final Vocabulary vocabulary;

    private final int[] vector;

    private final int size;

    private double[] weightedVector;

    private Weighting currentWeighting = Weighting.NONE;

    public BagOfWords(Vocabulary vocabulary, int[] vector, int size) {
        this.vocabulary = vocabulary;
        this.vector = vector;
        this.size = size;
        this.weightedVector = ArrayUtils.intToDoubleArray(vector);
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public int[] getVector() {
        return vector;
    }

    public int getSize() {
        return size;
    }

    public double[] getWeightedVector() {
        return weightedVector;
    }

    public Weighting getCurrentWeighting() {
        return currentWeighting;
    }

    public void applyWeighting(Weighting weighting) {
        if (weighting == currentWeighting || size == 0) {
            return;
        }

        currentWeighting = weighting;

        switch(weighting) {

            case NONE:
                weightedVector = ArrayUtils.intToDoubleArray(vector);
                return;

            case TF:
                weightedVector = new double[vector.length];

                for(int i = 0; i < vector.length; i++) {
                    weightedVector[i] = vector[i] / size;
                }
                return;

            case TFIDF:
                int totalWordCount = 0;
                for(Cluster c : vocabulary.getWords()) {
                    totalWordCount += c.getSize();
                }

                weightedVector = new double[vector.length];
                for (int i = 0; i < vector.length; i++) {
                    weightedVector[i] = (double) vector[i] / size
                            * Math.log((double) totalWordCount / (1 + (double) vocabulary.getWords().get(i).getSize()));
                }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BagOfWords that = (BagOfWords) o;

        if (size != that.size) return false;
        if (!vocabulary.equals(that.vocabulary)) return false;
        return Arrays.equals(vector, that.vector);

    }

    @Override
    public int hashCode() {
        int result = vocabulary.hashCode();
        result = 31 * result + Arrays.hashCode(vector);
        result = 31 * result + size;
        return result;
    }

    @Override
    public String toString() {
        return "BagOfWords{" +
                "vocabulary=" + vocabulary.getName() +
                ", vector=" + Arrays.toString(vector) +
                ", size=" + size +
                '}';
    }
}
