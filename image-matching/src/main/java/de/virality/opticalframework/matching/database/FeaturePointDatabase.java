package de.virality.opticalframework.matching.database;

import de.virality.opticalframework.core.feature.FeaturePoint;

import java.util.List;

public interface FeaturePointDatabase<T extends FeaturePoint> {

    void createImageSet(String path, String imageSetName);

    void saveFeaturePointsForImage(int imageId, List<T> featurePoints);

    List<DatabaseImage> getImagesForImageSet(String imageSetName);

    List<T> loadFeaturePointsForImage(int imageId);

    List<T> loadFeaturePointsForImageSet(String imageSetName);

    List<T> loadRandomFeaturePointsSampleForImageSet(String imageSetName, float percentage);
}
