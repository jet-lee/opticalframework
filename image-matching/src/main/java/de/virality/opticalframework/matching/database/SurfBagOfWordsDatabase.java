package de.virality.opticalframework.matching.database;

import de.virality.opticalframework.core.cluster.Cluster;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.matching.bow.BagOfWords;
import de.virality.opticalframework.matching.bow.Vocabulary;
import de.virality.opticalframework.matching.util.DbUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class SurfBagOfWordsDatabase extends SurfDatabase implements BagOfWordsDatabase<SurfFeaturePoint> {

    public SurfBagOfWordsDatabase(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public void createVocabulary(String vocabularyName, List<Cluster> clusters) {
        try {
            ensureConnected();
            ensureTablesCreated();

            int vocabularyId = writeVocabulary(vocabularyName, clusters.size());
            writeClusters(vocabularyId, clusters);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeIfNeeded();
        }
    }

    @Override
    public Vocabulary loadVocabulary(String vocabularyName) {
        List<Cluster> clusters = new ArrayList<>();

        String sql = "SELECT size, index, center FROM clusters WHERE vocabulary = ? ORDER BY index";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            int vocabularyId = getVocabularyId(vocabularyName);

            statement = connection.prepareStatement(sql);
            statement.setInt(1, vocabularyId);

            result = statement.executeQuery();
            while (result.next()) {
                int size = result.getInt("size");
                int index = result.getInt("index");

                Double[] array = (Double[]) result.getArray("center").getArray();
                double[] center = ArrayUtils.toPrimitive(array);

                clusters.add(new Cluster(size, index, center));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }

        return new Vocabulary(vocabularyName, clusters);
    }

    @Override
    public void saveBagOfWordsForImage(int imageId, BagOfWords bagOfWord) {
        String sql = "INSERT INTO bows(image, vocabulary, length, size, bow) VALUES(?, ?, ?, ?, ?)";

        PreparedStatement statement = null;
        try {
            ensureConnected();

            int vocabularyId = getVocabularyId(bagOfWord.getVocabulary().getName());

            statement = connection.prepareStatement(sql);
            statement.setInt(1, imageId);
            statement.setInt(2, vocabularyId);
            statement.setInt(3, bagOfWord.getVector().length);
            statement.setInt(4, bagOfWord.getSize());

            Array bowArray = connection.createArrayOf("integer", ArrayUtils.toObject(bagOfWord.getVector()));
            statement.setArray(5, bowArray);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            closeIfNeeded();
        }
    }

    @Override
    public BagOfWords getBagOfWordsForImage(int imageId, Vocabulary vocabulary) {
        String sql = "SELECT size, bow FROM bows WHERE image = ? AND vocabulary = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            int vocabularyId = getVocabularyId(vocabulary.getName());

            statement = connection.prepareStatement(sql);
            statement.setInt(1, imageId);
            statement.setInt(2, vocabularyId);

            result = statement.executeQuery();
            if (!result.next()) {
                throw new NoSuchElementException("No bag of words found for imageId: "
                        + imageId + " , vocabulary: " + vocabulary.getName());
            }

            int size = result.getInt("size");
            Integer[] array = (Integer[]) result.getArray("bow").getArray();
            int[] vector = ArrayUtils.toPrimitive(array);

            return new BagOfWords(vocabulary, vector, size);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }
    }

    @Override
    public List<BagOfWords> getBagOfWordsForImageSet(String imageSetName, Vocabulary vocabulary) {
        List<BagOfWords> bows = new ArrayList<>();

        String sql =
                "SELECT bows.size, bows.bow " +
                "FROM bows JOIN images ON bows.image = images.id " +
                "WHERE images.imageset = ? AND bows.vocabulary = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            int vocabularyId = getVocabularyId(vocabulary.getName());
            int imageSetId = getImageSetId(imageSetName);

            statement = connection.prepareStatement(sql);
            statement.setInt(1, imageSetId);
            statement.setInt(2, vocabularyId);

            result = statement.executeQuery();
            while (result.next()) {
                int size = result.getInt("size");
                Integer[] array = (Integer[]) result.getArray("bow").getArray();
                int[] vector = ArrayUtils.toPrimitive(array);

                bows.add(new BagOfWords(vocabulary, vector, size));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }

        return bows;
    }

    private int writeVocabulary(String name, int size) throws SQLException {
        String sql = "INSERT INTO vocabularies(name, size) VALUES (?, ?) RETURNING id";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.setInt(2, size);

            result = statement.executeQuery();
            result.next();
            return result.getInt("id");
        } finally {
            DbUtils.closeSilently(result);
            DbUtils.closeSilently(statement);
        }
    }

    private void writeClusters(int vocabularyId, List<Cluster> clusters) throws SQLException {
        String sql = "INSERT INTO clusters(vocabulary, size, index, center) VALUES(?, ?, ?, ?)";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);

            for (Cluster c : clusters) {
                statement.setInt(1, vocabularyId);
                statement.setInt(2, c.getSize());
                statement.setInt(3, clusters.indexOf(c));

                Array centerPoint = connection.createArrayOf("float", ArrayUtils.toObject(c.getCenter()));
                statement.setArray(4, centerPoint);

                statement.addBatch();
            }

            statement.executeBatch();
        } finally {
            DbUtils.closeSilently(statement);
        }
    }

    protected void ensureTablesCreated() throws SQLException {
        super.ensureTablesCreated();
        ensureVocabulariesTableCreated();
        ensureClustersTableCreated();
        ensureBoWsTableCreated();
    }

    private void ensureVocabulariesTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS vocabularies ("
                + "id SERIAL PRIMARY KEY,"
                + "name VARCHAR(100),"
                + "size INTEGER)";

        connection.createStatement().executeUpdate(sql);
    }

    private void ensureClustersTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS clusters ("
                + "id SERIAL PRIMARY KEY,"
                + "vocabulary INTEGER REFERENCES vocabularies(id) ON DELETE CASCADE,"
                + "size INTEGER,"
                + "index INTEGER,"
                + "center FLOAT[])";

        connection.createStatement().executeUpdate(sql);
    }

    private void ensureBoWsTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS bows ("
                + "id SERIAL PRIMARY KEY,"
                + "image INTEGER REFERENCES images(id) ON DELETE CASCADE,"
                + "vocabulary INTEGER REFERENCES vocabularies(id) ON DELETE CASCADE,"
                + "length INTEGER,"
                + "size INTEGER,"
                + "bow INTEGER[])";

        connection.createStatement().executeUpdate(sql);
    }

    protected int getVocabularyId(String name) throws SQLException {
        String sql = "SELECT id FROM vocabularies WHERE name = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, name);

            result = statement.executeQuery();
            if (!result.next()) {
                throw new NoSuchElementException("No vocabulary found with name " + name);
            }
            return result.getInt("id");
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
        }
    }
}
