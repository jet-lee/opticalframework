package de.virality.opticalframework.matching.bow;

public enum Weighting {
    NONE,
    TF,
    TFIDF
}
