package de.virality.opticalframework.matching.matcher;

import de.virality.opticalframework.core.cluster.Clusterable;
import de.virality.opticalframework.core.feature.FeaturePoint;
import de.virality.opticalframework.core.feature.FeaturePointExtractor;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.matcher.FeaturePointMatcher;
import de.virality.opticalframework.core.matcher.ParallelFeaturePointMatcher;
import de.virality.opticalframework.matching.bow.BagOfWords;
import de.virality.opticalframework.matching.bow.Weighting;
import de.virality.opticalframework.matching.database.RerankingDatabase;
import org.opencv.core.Mat;

import java.util.*;

public class RerankingMatcher<T extends FeaturePoint & Clusterable> extends BagOfWordsMatcher<T> {

    private final RerankingDatabase<T> database;

    private final List<Integer> stopList;

    private final int k;

    private final FeaturePointMatcher featurePointMatcher = new ParallelFeaturePointMatcher();

    public RerankingMatcher(RerankingDatabase<T> database, String imageSetName, String vocabularyName,
                            FeaturePointExtractor<T> featurePointExtractor, double q, int k) {
        this(database, imageSetName, vocabularyName, featurePointExtractor, q, k, Weighting.TFIDF, SimMeasure.COSINE);
    }

    public RerankingMatcher(RerankingDatabase<T> database, String imageSetName, String vocabularyName,
                            FeaturePointExtractor<T> featurePointExtractor, double q, int k, Weighting weighting,
                            SimMeasure simMeasure) {
        super(database, imageSetName, vocabularyName, featurePointExtractor, weighting, simMeasure);
        this.database = database;
        this.k = k;
        this.stopList = database.initSession(vocabularyName, q);
    }

    @Override
    public ImageMatch matchImage(Mat image) {
        final List<ImageMatch> matches = matchImageKnn(image, k);
        if (matches.isEmpty()) {
            return null;
        } else {
            return matches.get(0);
        }
    }

    @Override
    public List<ImageMatch> matchImageKnn(Mat image, int k) {
        if (k <= 0) {
            throw new IllegalArgumentException("k must be > 0");
        }

        final List<T> queryPoints = featurePointExtractor.extractFeaturePoints(image);
        HashMap<Integer, List<T>> wordMapping = new HashMap<>();
        final BagOfWords queryBoW = vocabulary.calculateBagOfWords(queryPoints, wordMapping);
        queryBoW.applyWeighting(weighting);

        List<ImageMatch> matches = matchBoWs(queryBoW);
        matches = matches.subList(0, Math.min(matches.size(), k));

        rerank(matches, wordMapping);

        return matches;
    }

    private void rerank(List<ImageMatch> matches, Map<Integer, List<T>> wordMapping) {
        List<T> rerankQueryPoints = new ArrayList<>();
        for (int i = 0; i < vocabulary.getWords().size(); i++) {
            if (!stopList.contains(i)) {
                rerankQueryPoints.addAll(wordMapping.getOrDefault(i, new ArrayList<>()));
            }
        }

        matches.parallelStream()
                .forEach(match -> {
                    List<T> currPoints = database.getRerankingFeaturePointsForImage(match.image.id);
                    List<FeaturePointMatch<T>> pointMatches = featurePointMatcher.findMatches(rerankQueryPoints, currPoints);
                    match.score = pointMatches.size();
                });

        Collections.sort(matches, Collections.reverseOrder());
    }

    @Override
    protected void finalize() throws Throwable {
        database.endSession();
        super.finalize();
    }
}
