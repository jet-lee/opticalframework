package de.virality.opticalframework.matching.matcher;

import de.virality.opticalframework.core.cluster.Clusterable;
import de.virality.opticalframework.core.feature.FeaturePoint;
import de.virality.opticalframework.core.feature.FeaturePointExtractor;
import de.virality.opticalframework.core.util.MathUtils;
import de.virality.opticalframework.matching.bow.BagOfWords;
import de.virality.opticalframework.matching.bow.Vocabulary;
import de.virality.opticalframework.matching.bow.Weighting;
import de.virality.opticalframework.matching.database.BagOfWordsDatabase;
import de.virality.opticalframework.matching.database.DatabaseImage;
import org.opencv.core.Mat;

import java.util.*;

public class BagOfWordsMatcher<T extends FeaturePoint & Clusterable> implements ImageMatcher {

    public enum SimMeasure {
        EUCLIDEAN, COSINE
    }

    protected FeaturePointExtractor<T> featurePointExtractor;

    protected final Vocabulary vocabulary;

    protected Map<DatabaseImage, BagOfWords> databaseBoWs;

    protected Weighting weighting;

    protected SimMeasure simMeasure;

    public BagOfWordsMatcher(BagOfWordsDatabase<T> database, String imageSetName, String vocabularyName,
                             FeaturePointExtractor<T> featurePointExtractor) {
        this(database, imageSetName, vocabularyName, featurePointExtractor, Weighting.TFIDF, SimMeasure.COSINE);
    }

    public BagOfWordsMatcher(BagOfWordsDatabase<T> database, String imageSetName, String vocabularyName,
                             FeaturePointExtractor<T> featurePointExtractor, Weighting weighting, SimMeasure simMeasure) {
        this.featurePointExtractor = featurePointExtractor;
        this.vocabulary = database.loadVocabulary(vocabularyName);
        this.weighting = weighting;
        this.simMeasure = simMeasure;
        prepare(database, imageSetName);
    }

    private void prepare(BagOfWordsDatabase<T> database, String imageSetName) {
        databaseBoWs = new HashMap<>();

        final List<DatabaseImage> images = database.getImagesForImageSet(imageSetName);
        for (DatabaseImage img : images) {
            BagOfWords bow = database.getBagOfWordsForImage(img.id, vocabulary);
            if (bow.getSize() == 0) {
                continue;
            }
            bow.applyWeighting(weighting);
            databaseBoWs.put(img, bow);
        }
    }

    public FeaturePointExtractor<T> getFeaturePointExtractor() {
        return featurePointExtractor;
    }

    public void setFeaturePointExtractor(FeaturePointExtractor<T> featurePointExtractor) {
        this.featurePointExtractor = featurePointExtractor;
    }

    public Weighting getWeighting() {
        return weighting;
    }

    public void setWeighting(Weighting weighting) {
        if (this.weighting != weighting) {
            this.weighting = weighting;
            for (BagOfWords bow : databaseBoWs.values()) {
                bow.applyWeighting(weighting);
            }
        }
    }

    public SimMeasure getSimMeasure() {
        return simMeasure;
    }

    public void setSimMeasure(SimMeasure simMeasure) {
        this.simMeasure = simMeasure;
    }

    @Override
    public ImageMatch matchImage(Mat image) {
        final List<ImageMatch> matches = matchImageKnn(image, 1);
        if (matches.isEmpty()) {
            return null;
        } else {
            return matches.get(0);
        }
    }

    @Override
    public List<ImageMatch> matchImageKnn(Mat image, int k) {
        if (k <= 0) {
            throw new IllegalArgumentException("k must be > 0");
        }

        final List<T> queryPoints = featurePointExtractor.extractFeaturePoints(image);
        final BagOfWords queryBoW = vocabulary.calculateBagOfWords(queryPoints);
        queryBoW.applyWeighting(weighting);

        final List<ImageMatch> matches = matchBoWs(queryBoW);

        return matches.subList(0, Math.min(matches.size(), k));
    }

    protected List<ImageMatch> matchBoWs(BagOfWords queryBoW) {
        final List<ImageMatch> matches = new ArrayList<>();
        for (Map.Entry<DatabaseImage, BagOfWords> entry : databaseBoWs.entrySet()) {
            if (!isCandidate(entry.getKey())) {
                continue;
            }

            double score = getDistance(queryBoW, entry.getValue());

            ImageMatch newMatch = new ImageMatch(entry.getKey(), score);
            matches.add(newMatch);
        }

        if (simMeasure == SimMeasure.COSINE) {
            // For cosine similarity, greater values are better, so we want the reverse order
            Collections.sort(matches, Collections.reverseOrder());
        } else {
            Collections.sort(matches);
        }
        return matches;
    }

    protected boolean isCandidate(DatabaseImage img) {
        return true;
    }

    private double getDistance(BagOfWords bow1, BagOfWords bow2) {
        if (simMeasure == SimMeasure.COSINE) {
            return MathUtils.cosineSimilarity(bow1.getWeightedVector(), bow2.getWeightedVector());
        } else {
            return MathUtils.euclideanDistance(bow1.getWeightedVector(), bow2.getWeightedVector());
        }
    }
}
