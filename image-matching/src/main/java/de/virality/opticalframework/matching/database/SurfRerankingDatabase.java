package de.virality.opticalframework.matching.database;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.matching.bow.Vocabulary;
import de.virality.opticalframework.matching.util.DbUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SurfRerankingDatabase extends SurfBagOfWordsDatabase implements RerankingDatabase<SurfFeaturePoint> {

    private boolean initialized = false;

    public SurfRerankingDatabase(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public void createPointWordMappingForImageSet(String imageSetName, String vocabularyName) {
        String sql = "SELECT surfpoints.id, surfpoints.descriptor FROM surfpoints " +
                "JOIN images ON surfpoints.image = images.id WHERE images.imageset = ?";

        String batchSql = "INSERT INTO pointword(surfpoint, vocabulary, wordindex) VALUES (?, ?, ?)";

        Vocabulary vocabulary = loadVocabulary(vocabularyName);

        PreparedStatement getPointsStatement = null;
        PreparedStatement batchInsertStatement = null;
        ResultSet result = null;
        try {
            ensureConnected();
            ensureTablesCreated();

            int imageSetId = getImageSetId(imageSetName);
            int vocabularyId = getVocabularyId(vocabularyName);

            getPointsStatement = connection.prepareStatement(sql);
            getPointsStatement.setInt(1, imageSetId);

            batchInsertStatement = connection.prepareStatement(batchSql);

            result = getPointsStatement.executeQuery();
            while (result.next()) {
                int pointId = result.getInt("id");
                Double[] array = (Double[]) result.getArray("descriptor").getArray();
                double[] descriptor = ArrayUtils.toPrimitive(array);

                int wordIndex = vocabulary.getWordForPoint(descriptor).getIndex();

                batchInsertStatement.setInt(1, pointId);
                batchInsertStatement.setInt(2, vocabularyId);
                batchInsertStatement.setInt(3, wordIndex);
                batchInsertStatement.addBatch();
            }

            batchInsertStatement.executeBatch();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(getPointsStatement);
            DbUtils.closeSilently(batchInsertStatement);
            DbUtils.closeSilently(result);
            closeIfNeeded();
        }
    }

    @Override
    public List<Integer> initSession(String vocabularyName, double q) {
        List<Integer> stopList = new ArrayList<>();

        String sql =
                "SELECT image, wordindex, x, y, scale, orientation, descriptor " +
                "INTO TEMPORARY tmp_rerank_helper " +
                "FROM surfpoints JOIN pointword ON surfpoints.id=pointword.surfpoint " +
                "WHERE vocabulary = ? AND NOT (wordindex = ANY (?))";

        String sqlIndex = "CREATE INDEX ON tmp_rerank_helper (image)";

        PreparedStatement statement = null;
        try {
            ensureConnected();

            int vocabularyId = getVocabularyId(vocabularyName);
            stopList = getStopList(vocabularyId, q);

            statement = connection.prepareStatement(sql);
            statement.setInt(1, vocabularyId);

            Array stopIndices = connection.createArrayOf("integer", stopList.toArray());
            statement.setArray(2, stopIndices);

            statement.execute();
            connection.createStatement().executeUpdate(sqlIndex);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
        }

        initialized = true;
        setAutoClose(false);
        return stopList;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<SurfFeaturePoint> getRerankingFeaturePointsForImage(int imgId) {
        if (!initialized) {
            throw new IllegalStateException("Reranking is not initialized, call initSession() first!");
        }

        List<SurfFeaturePoint> points = new ArrayList<>();

        String sql = "SELECT x, y, scale, orientation, descriptor FROM tmp_rerank_helper WHERE image = ?";

        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            ensureConnected();

            statement = connection.prepareStatement(sql);
            statement.setInt(1, imgId);

            result = statement.executeQuery();
            while (result.next()) {
                points.add(getFeaturePointForRow(result));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeSilently(statement);
            DbUtils.closeSilently(result);
        }

        return points;
    }

    @Override
    public void endSession() {
        DbUtils.closeSilently(connection);
        initialized = false;
        setAutoClose(true);
    }

    private List<Integer> getStopList(int vocId, double q) throws SQLException {
        List<Integer> stopList = new ArrayList<>();

        String sql1 = "SELECT sum(size) FROM clusters WHERE vocabulary = ?";
        String sql2 = "SELECT id, size, index FROM clusters WHERE vocabulary = ? ORDER BY size DESC";

        PreparedStatement statement1 = null;
        PreparedStatement statement2 = null;
        ResultSet result1 = null;
        ResultSet result2 = null;
        try {
            statement1 = connection.prepareStatement(sql1);
            statement1.setInt(1, vocId);

            result1 = statement1.executeQuery();
            result1.next();
            int totalCount = result1.getInt(1);

            statement2 = connection.prepareStatement(sql2);
            statement2.setInt(1, vocId);

            result2 = statement2.executeQuery();
            int sumPoints = 0;
            while (result2.next()) {
                int size = result2.getInt("size");
                int index = result2.getInt("index");

                stopList.add(index);
                sumPoints += size;

                if (sumPoints > totalCount * q) {
                    break;
                }
            }

            return stopList;
        } finally {
            DbUtils.closeSilently(statement1);
            DbUtils.closeSilently(statement2);
            DbUtils.closeSilently(result1);
            DbUtils.closeSilently(result2);
        }
    }

    protected void ensureTablesCreated() throws SQLException {
        super.ensureTablesCreated();
        ensurePointWordTableCreated();
    }

    private void ensurePointWordTableCreated() throws SQLException {
        String sql =
                "CREATE TABLE IF NOT EXISTS pointword ("
                        + "id SERIAL PRIMARY KEY,"
                        + "surfpoint INTEGER REFERENCES surfpoints(id) ON DELETE CASCADE,"
                        + "vocabulary INTEGER REFERENCES vocabularies(id) ON DELETE CASCADE,"
                        + "wordindex INTEGER)";

        connection.createStatement().executeUpdate(sql);
    }
}
