package de.virality.opticalframework.matching.database;

import de.virality.opticalframework.matching.bow.Vocabulary;
import de.virality.opticalframework.core.cluster.Cluster;
import de.virality.opticalframework.matching.bow.BagOfWords;
import de.virality.opticalframework.core.feature.FeaturePoint;

import java.util.List;

public interface BagOfWordsDatabase<T extends FeaturePoint> extends FeaturePointDatabase<T> {

    void createVocabulary(String vocabularyName, List<Cluster> clusters);

    Vocabulary loadVocabulary(String vocabularyName);

    void saveBagOfWordsForImage(int imageId, BagOfWords bagOfWord);

    BagOfWords getBagOfWordsForImage(int imageId, Vocabulary vocabulary);

    List<BagOfWords> getBagOfWordsForImageSet(String imageSetName, Vocabulary vocabulary);
}
