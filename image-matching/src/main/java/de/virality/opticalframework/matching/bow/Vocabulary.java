package de.virality.opticalframework.matching.bow;

import de.virality.opticalframework.core.cluster.Cluster;
import de.virality.opticalframework.core.cluster.Clusterable;
import de.virality.opticalframework.core.util.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Vocabulary {

    private final String name;

    private final List<Cluster> words;

    public Vocabulary(String name, List<Cluster> words) {
        this.name = name;
        this.words = words;
    }

    public String getName() {
        return name;
    }

    public List<Cluster> getWords() {
        return words;
    }

    public BagOfWords calculateBagOfWords(List<? extends Clusterable> points) {
        return calculateBagOfWords(points, null);
    }

    public <T extends Clusterable> BagOfWords calculateBagOfWords(List<T> points, Map<Integer, List<T>> mappingOut) {
        List<Integer> indexes = points.parallelStream()
                .map(this::getWordForPoint)
                .map(Cluster::getIndex)
                .collect(Collectors.toList());

        int[] bow = new int[words.size()];
        for (int i = 0; i < indexes.size(); i++) {
            Integer wordIndex = indexes.get(i);
            bow[wordIndex]++;

            if (mappingOut != null) {
                List<T> mappedPoints = mappingOut.getOrDefault(wordIndex, new ArrayList<>());
                mappedPoints.add(points.get(i));
                mappingOut.put(wordIndex, mappedPoints);
            }
        }

        return new BagOfWords(this, bow, points.size());
    }

    public <T extends Clusterable> Map<T, Integer> calculatePointWordMapping(List<T> points) {
        Map<T, Integer> mapping = new HashMap<>();

        for (int i = 0; i < points.size(); i++) {
            T point = points.get(i);
            Cluster nearestCluster = getWordForPoint(point);
            mapping.put(point, nearestCluster.getIndex());
        }

        return mapping;
    }

    public Cluster getWordForPoint(Clusterable point) {
        return getWordForPoint(point.getPoint());
    }

    public Cluster getWordForPoint(double[] point) {
        double bestDist = Float.MAX_VALUE;
        Cluster result = null;

        for (int i = 0; i < words.size(); i++) {
            double currentDist = MathUtils.euclideanDistance(point, words.get(i).getCenter());

            if (currentDist < bestDist) {
                bestDist = currentDist;
                result = words.get(i);
            }
        }

        return result;
    }
}
