package de.virality.opticalframework.matching.matcher;

import de.virality.opticalframework.matching.database.DatabaseImage;

public class ImageMatch implements Comparable<ImageMatch> {

    public DatabaseImage image;

    public double score;

    public ImageMatch(DatabaseImage image, double score) {
        this.image = image;
        this.score = score;
    }

    @Override
    public int compareTo(ImageMatch o) {
        return new Double(score).compareTo(o.score);
    }
}
