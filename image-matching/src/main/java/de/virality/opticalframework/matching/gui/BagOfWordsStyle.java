package de.virality.opticalframework.matching.gui;

import de.virality.opticalframework.matching.bow.Vocabulary;
import de.virality.opticalframework.core.feature.FeaturePoint;
import de.virality.opticalframework.core.gui.drawer.FeaturePointDrawer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BagOfWordsStyle implements FeaturePointDrawer.Style {

    private static final Color[] DEFAULT_COLORS = new Color[] {
        Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.MAGENTA, Color.CYAN, Color.BLACK, Color.ORANGE
    };

    private static final int STROKE_WIDTH = 2;

    private static final int CIRCLE_RADIUS = 4;

    private final Stroke stroke = new BasicStroke(STROKE_WIDTH);

    private final Map<? extends FeaturePoint, Integer> mapping;

    private final List<Color> colors;

    public BagOfWordsStyle(Vocabulary vocabulary, Map<? extends FeaturePoint, Integer> pointWordMapping) {
        this.mapping = pointWordMapping;
        this.colors = new ArrayList<>(vocabulary.getWords().size());
        generateColors(vocabulary);
    }

    private void generateColors(Vocabulary vocabulary) {
        final Random random = new Random();
        for (int i = 0; i < vocabulary.getWords().size(); i++) {
            if (i < DEFAULT_COLORS.length) {
                colors.add(DEFAULT_COLORS[i]);
            } else {
                // TODO: improve color generator
                int rgb = Color.HSBtoRGB(random.nextFloat(), 1f, 1f);
                colors.add(new Color(rgb));
            }
        }
    }

    @Override
    public void onDrawFeature(Graphics2D graphics2D, FeaturePoint featurePoint, double scaleX, double scaleY) {
        graphics2D.setStroke(stroke);

        int x = (int) (featurePoint.getX() * scaleX - CIRCLE_RADIUS);
        int y = (int) (featurePoint.getY() * scaleY - CIRCLE_RADIUS);

        int colorIndex = mapping.get(featurePoint);
        graphics2D.setPaint(colors.get(colorIndex));

        graphics2D.drawOval(x, y, CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2);
    }
}
