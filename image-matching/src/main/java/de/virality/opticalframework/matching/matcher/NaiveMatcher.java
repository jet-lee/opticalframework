package de.virality.opticalframework.matching.matcher;

import de.virality.opticalframework.core.feature.FeaturePointExtractor;
import de.virality.opticalframework.core.feature.FeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.matcher.FeaturePointMatcher;
import de.virality.opticalframework.core.matcher.SimpleFeaturePointMatcher;
import de.virality.opticalframework.matching.database.DatabaseImage;
import de.virality.opticalframework.matching.database.FeaturePointDatabase;
import org.opencv.core.Mat;

import java.util.*;

public class NaiveMatcher<T extends FeaturePoint> implements ImageMatcher {

    private FeaturePointExtractor<T> featurePointExtractor;

    private FeaturePointMatcher featurePointMatcher;

    private Map<DatabaseImage, List<T>> databasePoints;

    private float matchingThreshold = SimpleFeaturePointMatcher.DEFAULT_THRESHOLD;

    public NaiveMatcher(FeaturePointDatabase<T> trainingDatabase, String imageSetName,
                        FeaturePointExtractor<T> featurePointExtractor, FeaturePointMatcher featurePointMatcher) {
        this.featurePointExtractor = featurePointExtractor;
        this.featurePointMatcher = featurePointMatcher;
        prepare(trainingDatabase, imageSetName);
    }

    private void prepare(FeaturePointDatabase<T> database, String imageSetName) {
        databasePoints = new HashMap<>();

        final List<DatabaseImage> images = database.getImagesForImageSet(imageSetName);
        for (DatabaseImage img : images) {
            List<T> featurePoints = database.loadFeaturePointsForImage(img.id);
            if (featurePoints.size() < 2) {
                continue;
            }
            databasePoints.put(img, featurePoints);
        }
    }

    public void setFeaturePointExtractor(FeaturePointExtractor<T> featurePointExtractor) {
        this.featurePointExtractor = featurePointExtractor;
    }

    public FeaturePointExtractor<T> getFeaturePointExtractor() {
        return featurePointExtractor;
    }

    public float getMatchingThreshold() {
        return matchingThreshold;
    }

    public void setMatchingThreshold(float threshold) {
        this.matchingThreshold = threshold;
    }

    @Override
    public ImageMatch matchImage(Mat image) {
        final List<ImageMatch> matches =  matchImageKnn(image, 1);
        if (matches.isEmpty()) {
            return null;
        } else {
            return matches.get(0);
        }
    }

    @Override
    public List<ImageMatch> matchImageKnn(Mat image, int k) {
        if (k == 0) {
            throw new IllegalArgumentException("k must be > 0");
        }

        final List<T> queryPoints = featurePointExtractor.extractFeaturePoints(image);

        final List<ImageMatch> matches = new ArrayList<>();
        for (Map.Entry<DatabaseImage, List<T>> entry : databasePoints.entrySet()) {
            if (!isCandidate(entry.getKey())) {
                continue;
            }

            List<FeaturePointMatch<T>> pointMatches = featurePointMatcher.findMatches(queryPoints, entry.getValue());

            int score = pointMatches.size();
            if (score == 0) {
                continue;
            }

            ImageMatch newMatch = new ImageMatch(entry.getKey(), score);
            matches.add(newMatch);
        }

        Collections.sort(matches, Collections.reverseOrder());

        return matches.subList(0, Math.min(matches.size(), k));
    }

    protected boolean isCandidate(DatabaseImage img) {
        return true;
    }
}
