package de.virality.opticalframework.core.gui.drawer;

import de.virality.opticalframework.core.feature.FeaturePoint;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;

import java.awt.*;
import java.util.List;

/**
 * A {@link Drawer} that draws {@link FeaturePoint}s on an image.
 * <p>
 * You can supply a custom {@link Style} via {@link #setStyle(Style)} to control how a {@link FeaturePoint} is drawn,
 * otherwise a default one will be used.
 *
 * @param <T> the feature point type
 */
public class FeaturePointDrawer<T extends FeaturePoint> implements Drawer {

    private List<T> featurePoints;

    private Style<T> drawer;

    public FeaturePointDrawer() {
        this(null);
    }

    public FeaturePointDrawer(List<T> featurePoints) {
        this(featurePoints, Style.getDefault());
    }

    public FeaturePointDrawer(List<T> featurePoints, Style<T> pointStyle) {
        this.featurePoints = featurePoints;
        this.drawer = pointStyle;
    }

    public void setFeaturePoints(List<T> featurePoints) {
        this.featurePoints = featurePoints;
    }

    public void setStyle(Style<T> drawer) {
        this.drawer = drawer;
    }

    @Override
    public void draw(Graphics2D graphics2D, double scaleX, double scaleY) {
        if (featurePoints != null) {
            for (T point : featurePoints) {
                drawer.onDrawFeature(graphics2D, point, scaleX, scaleY);
            }
        }
    }

    /**
     * Draws a single {@link FeaturePoint}.
     * <p>
     * You can implement this interface to customize the way {@link FeaturePoint}s are drawn when using a
     * {@link FeaturePointDrawer}.
     *
     * @param <T> the feature point type
     */
    public interface Style<T extends FeaturePoint> {

        static <F extends FeaturePoint> Style<F> getDefault() {
            return new SingleColorStyle<>(Color.GREEN);
        }

        Style<SurfFeaturePoint> SURF = new SurfStyle();

        void onDrawFeature(Graphics2D graphics2D, T featurePoint, double scaleX, double scaleY);
    }
}
