package de.virality.opticalframework.core.util;

import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;

public final class VideoUtils {

    // Due to a bug in OpenCV Java (see https://github.com/opencv/opencv/issues/4671), most of the CV_CAP_PROP_*
    // constants are missing, so we list them here (see http://stackoverflow.com/a/21066952)
    public static final int CV_CAP_PROP_FRAME_WIDTH = Highgui.CV_CAP_PROP_FRAME_WIDTH;
    public static final int CV_CAP_PROP_FRAME_HEIGHT = Highgui.CV_CAP_PROP_FRAME_HEIGHT;
    public static final int CV_CAP_PROP_FPS = 5;
    public static final int CV_CAP_PROP_FRAME_COUNT = 6;

    private VideoUtils() { }

    public static double getFramesPerSecond(VideoCapture capture) {
        return capture.get(CV_CAP_PROP_FPS);
    }

    public static int getFrameCount(VideoCapture capture) {
        return (int) capture.get(CV_CAP_PROP_FRAME_COUNT);
    }

    public static int getHorizontalResolution(VideoCapture capture) {
        return (int) capture.get(CV_CAP_PROP_FRAME_WIDTH);
    }

    public static int getVerticalResolution(VideoCapture capture) {
        return (int) capture.get(CV_CAP_PROP_FRAME_HEIGHT);
    }

    /**
     * Common wide screen video resolutions.
     */
    public enum Res16x9 {

        _180(320, 180),
        _360(640, 360),
        _720(1280, 720),
        _1080(1920, 1080);

        public int width;
        public int height;

        Res16x9(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public Size getSize() {
            return new Size(width, height);
        }
    }
}
