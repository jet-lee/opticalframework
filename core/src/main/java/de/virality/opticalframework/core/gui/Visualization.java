package de.virality.opticalframework.core.gui;

import javax.swing.*;

/**
 * A base class to enable the creation of a GUI that is placed inside a window and can be shown to the user.
 * <p>
 * Subclasses should implement {@link #getComponent()} to provide the actual content of the window.
 */
public abstract class Visualization {

    private JFrame window;

    private JComponent content;

    private int closeOperation = WindowConstants.DISPOSE_ON_CLOSE;

    /**
     * Set the behavior when the window is closed by the user
     * @param closeOperation one of the constants defined in the {@link WindowConstants} class
     */
    public void setCloseOperation(int closeOperation) {
        this.closeOperation = closeOperation;
    }

    public void show() {
        show("OpticalFramework");
    }

    public void show(String title) {
        window = new JFrame();

        content = getComponent();

        window.setTitle(title);
        window.setDefaultCloseOperation(closeOperation);
        window.setResizable(true);
        window.add(content);
        window.pack();
        window.setVisible(true);
    }

    /**
     * @return true, if {@link #show()} has been called and the window is currently visible
     */
    public boolean isShown() {
        return window != null && window.isVisible();
    }

    /**
     * Repaints the window contents. You might have to call this whenever you make changes to the visualized data.
     */
    public void refresh() {
        if (content != null && isShown()) {
            content.repaint();
        }
    }

    protected abstract JComponent getComponent();
}
