package de.virality.opticalframework.core.cluster;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Clusterer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A helper class to simplify the usage of the various {@link Clusterer} implementations of ApacheCommonsMath3.
 * <p>
 * To create a new {@link Clustering} that is backed by an Apache Commons {@link Clusterer}, just implement the
 * {@link #createClusterer()} method and return the {@link Clusterer} instance.
 */
public abstract class ApacheCommonsClustererAdapter implements Clustering {

    protected abstract <T extends org.apache.commons.math3.ml.clustering.Clusterable> Clusterer<T> createClusterer();

    @Override
    public List<Cluster> calculateClusters(List<? extends Clusterable> points) {
        Clusterer<Item> clusterer = createClusterer();
        List<? extends org.apache.commons.math3.ml.clustering.Cluster<Item>> clusters = clusterer.cluster(wrap(points));
        return unwrap(clusters);
    }

    private List<Item> wrap(List<? extends Clusterable> input) {
        return input.stream().map(Item::new).collect(Collectors.toList());
    }

    private List<Cluster> unwrap(List<? extends org.apache.commons.math3.ml.clustering.Cluster> clusters) {
        List<Cluster> out = new ArrayList<>(clusters.size());
        for (int i = 0; i < clusters.size(); i++) {
            org.apache.commons.math3.ml.clustering.Cluster c = clusters.get(i);

            double[] center;
            if (c instanceof CentroidCluster) {
                center = ((CentroidCluster) c).getCenter().getPoint();
            } else {
                center = getClusterCenter(c);
            }

            out.add(new Cluster(c.getPoints().size(), i, center));
        }

        return out;
    }

    private double[] getClusterCenter(org.apache.commons.math3.ml.clustering.Cluster c) {
        int x = 0;
        int y = 0;
        for (Object i : c.getPoints()) {
            org.apache.commons.math3.ml.clustering.Clusterable item =
                    (org.apache.commons.math3.ml.clustering.Clusterable) i;
            x += item.getPoint()[0];
            y += item.getPoint()[1];
        }

        return new double[] {x / c.getPoints().size(), y / c.getPoints().size()};
    }

    private static class Item implements org.apache.commons.math3.ml.clustering.Clusterable {

        private double[] point;

        Item(Clusterable in) {
            this.point = in.getPoint();
        }

        @Override
        public double[] getPoint() {
            return point;
        }
    }

}
