package de.virality.opticalframework.core.util;

import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;

public final class ImageUtils {

    private ImageUtils() { }

    /**
     * Try to load an image from the specified path.
     *
     * @param path the path to the image file
     * @return a {@link Mat} containing the loaded image
     * @throws IllegalArgumentException if the file does not exist
     */
    public static Mat loadImageFromPath(String path) {
        if (!new File(path).exists()) {
            throw new IllegalArgumentException("Image path does not exist! " + path);
        }

        return Highgui.imread(path);
    }

    /**
     * Converts a {@link Mat} to a {@link BufferedImage}.
     */
    public static BufferedImage matToBufferedImage(Mat m) {
        // source: http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b);
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);

        return image;
    }
}
