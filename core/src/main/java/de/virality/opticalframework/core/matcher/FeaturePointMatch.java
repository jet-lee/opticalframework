package de.virality.opticalframework.core.matcher;

import de.virality.opticalframework.core.feature.FeaturePoint;

/**
 * Represents a pair of matching {@link FeaturePoint}s that were found using a {@link FeaturePointMatcher}.
 *
 * @param <T> The feature point type
 */
public class FeaturePointMatch<T extends FeaturePoint> {

    public T first;

    public T second;

    public double score;

    public FeaturePointMatch(T first, T second, double score) {
        this.first = first;
        this.second = second;
        this.score = score;
    }
}
