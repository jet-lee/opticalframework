package de.virality.opticalframework.core.feature;

/**
 * Common base class for all visual feature points.
 * <p>
 * Feature points are obtained by running a {@link FeaturePointExtractor} on an image.
 *
 * @param <T> The type of FeaturePoints that can be matched with instances of this class.
 *           Since it is intended that points can be matched with points of the same type, one should just pass the type
 *           of the concrete subclass.
 */
public abstract class FeaturePoint<T extends FeaturePoint<T>> {
    protected double x;
    protected double y;

    public FeaturePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return The x-coordinate of the feature point location
     */
    public double getX() {
        return x;
    }

    /**
     * @return @return The y-coordinate of the feature point location
     */
    public double getY() {
        return y;
    }

    /**
     * Calculates the distance to another feature point in the sense of a similarity score.
     *
     * The concrete implementation depends on the subclass. Typically, this would consider some kind of descriptor and
     * return the distance of the two descriptors using a suitable distance measure. Smaller values indicate higher
     * similarity.
     *
     * @param another a feature point of the same type
     * @return the distance to this feature point in the sense of a similarity score - smaller values indicate higher
     *         similarity.
     */
    public abstract double matchWith(T another);
}
