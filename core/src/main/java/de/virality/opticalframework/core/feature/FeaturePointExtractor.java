package de.virality.opticalframework.core.feature;

import org.opencv.core.Mat;

import java.util.List;

/**
 * Common interface for all algorithms that extract visual feature points from an image.
 *
 * @param <T> the type of feature points the algorithm will extract
 */
public interface FeaturePointExtractor<T extends FeaturePoint> {

    /**
     * Extracts visual feature points from an image.
     *
     * @param img the image to extract features from
     * @return a list of feature points that were detected
     */
    List<T> extractFeaturePoints(Mat img);
}
