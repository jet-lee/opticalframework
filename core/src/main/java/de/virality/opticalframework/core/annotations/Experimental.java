package de.virality.opticalframework.core.annotations;

import java.lang.annotation.*;

/**
 * Marks a class, method or package as experimental, meaning it may contain unstable, untested or otherwise experimental
 * code or features.
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PACKAGE})
public @interface Experimental {

    /** An optional comment. */
    String value() default "";
}
