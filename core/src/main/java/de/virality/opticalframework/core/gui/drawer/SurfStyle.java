package de.virality.opticalframework.core.gui.drawer;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;

import java.awt.*;

/**
 * A default {@link FeaturePointDrawer.Style} that draws a {@link SurfFeaturePoint} as defined in the OpenSurf library.
 * <p>
 * This style is supposed to visualize the different attributes of a {@link SurfFeaturePoint}:
 * The point is represented by a circle around the point coordinates. The radius of the circle indicates the scale of
 * the point. Orientation is denoted by a green line from the center of the circle, while upright points are shown with
 * a green cross instead. Circle color depends on the laplacian value: blue means dark blobs on light backgrounds
 * (laplacian=1) and red being light blobs on dark backgrounds (laplacian=0).
 */
public class SurfStyle implements FeaturePointDrawer.Style<SurfFeaturePoint> {

    private static final int STROKE_WIDTH = 1;

    private Stroke stroke = new BasicStroke(STROKE_WIDTH);

    @Override
    public void onDrawFeature(Graphics2D graphics2D, SurfFeaturePoint featurePoint, double scaleX, double scaleY) {
        graphics2D.setStroke(stroke);

        // from OpenSurf/utils.cpp
        double scale = 2.5 * featurePoint.getScale();
        double orientation = featurePoint.getOrientation();
        int laplacian = featurePoint.getLaplacian();
        int x = (int) (featurePoint.getX() * scaleX);
        int y = (int) (featurePoint.getY() * scaleY);

        graphics2D.setPaint(Color.GREEN);
        if (orientation != 0) {
            // Green line indicates orientation
            int x2 = (int) ((scale * Math.cos(orientation)) + x);
            int y2 = (int) ((scale * Math.sin(orientation)) + y);
            graphics2D.drawLine(x, y, x2, y2);
        } else {
            // Green dot if using upright version
            graphics2D.drawLine(x - 2, y, x + 2, y);
            graphics2D.drawLine(x, y - 2, x, y + 2);
        }

        if (laplacian == 1) {
            // Blue circles indicate dark blobs on light backgrounds
            graphics2D.setPaint(Color.BLUE);
        } else {
            // Red circles indicate light blobs on dark backgrounds
            graphics2D.setPaint(Color.RED);
        }

        int centerX = (int) (x - scale);
        int centerY = (int) (y - scale);
        int size = (int) (scale * 2);
        graphics2D.drawOval(centerX, centerY, size, size);
    }
}
