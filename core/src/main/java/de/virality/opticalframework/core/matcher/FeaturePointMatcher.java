package de.virality.opticalframework.core.matcher;

import de.virality.opticalframework.core.feature.FeaturePoint;

import java.util.List;

/**
 * A common interface for finding {@link FeaturePointMatch}es between two lists of {@link FeaturePoint}s.
 */
public interface FeaturePointMatcher {

    /**
     * Finds matching {@link FeaturePoint} pairs from two distinctive {@link FeaturePoint} sets.
     * Which point pairs are considered as matches depends on the implementation.
     *
     * @param points1 A list of {@link FeaturePoint}s
     * @param points2 A list of {@link FeaturePoint}s to match with
     * @param <T> The feature point type
     * @return A list of {@link FeaturePointMatch}es
     */
    <T extends FeaturePoint<T>> List<FeaturePointMatch<T>> findMatches(List<T> points1, List<T> points2);
}
