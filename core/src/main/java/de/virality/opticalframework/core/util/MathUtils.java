package de.virality.opticalframework.core.util;

public final class MathUtils {

    private MathUtils() { }

    public static double euclideanDistance(double[] v1, double[] v2) {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("Can't calculate euclidean distance of vectors with different dimensions");
        }

        float sum = 0;
        for (int i = 0; i < v1.length; i++) {
            sum += Math.pow(v1[i] - v2[i], 2);
        }
        return (float) Math.sqrt(sum);
    }

    public static double cosineSimilarity(double[] v1, double[] v2) {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("Can't calculate cosine similarity of vectors with different dimensions");
        }

        double dotprod = 0.0;
        double mag1 = 0.0;
        double mag2 = 0.0;

        for (int i = 0; i < v1.length; i++) {
            dotprod += v1[i] * v2[i];
            mag1 += Math.pow(v1[i], 2);
            mag2 += Math.pow(v2[i], 2);
        }

        return dotprod / (Math.sqrt(mag1) * Math.sqrt(mag2));
    }
}
