package de.virality.opticalframework.core.cluster;

import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.Clusterer;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

/**
 * {@link Clustering} based on David Arthur and Sergei Vassilvitski k-means++ algorithm.
 * <p>
 * This is a wrapper around Apache Commons {@link KMeansPlusPlusClusterer}
 *
 * @see KMeansPlusPlusClusterer
 */
public class KMeansPlusPlusClustering extends ApacheCommonsClustererAdapter {

    public static int DEFAULT_K = 4;

    public static int DEFAULT_MAX_ITERATIONS = 10;

    private int k;

    private int maxIterations;

    public KMeansPlusPlusClustering() {
        this(DEFAULT_K, DEFAULT_MAX_ITERATIONS);
    }

    public KMeansPlusPlusClustering(int k, int maxIterations) {
        this.k = k;
        this.maxIterations = maxIterations;
    }

    @Override
    protected <T extends Clusterable> Clusterer<T> createClusterer() {
        return new KMeansPlusPlusClusterer<>(k, maxIterations);
    }
}
