package de.virality.opticalframework.core.matcher;

import de.virality.opticalframework.core.feature.FeaturePoint;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A parallel implementation of {@link SimpleFeaturePointMatcher} using multiple threads for speeding up the matching
 * process.
 */
public class ParallelFeaturePointMatcher extends SimpleFeaturePointMatcher {

    public static final float DEFAULT_THRESHOLD = 0.65f;

    public ParallelFeaturePointMatcher() {
        this(DEFAULT_THRESHOLD);
    }

    public ParallelFeaturePointMatcher(float threshold) {
        super(threshold);
    }

    @Override
    public <T extends FeaturePoint<T>> List<FeaturePointMatch<T>> findMatches(List<T> points1, List<T> points2) {
        return points1.parallelStream()
                .map(p -> findMatchForPoint(p, points2))
                .filter(m -> m != null)
                .collect(Collectors.toList());
    }
}
