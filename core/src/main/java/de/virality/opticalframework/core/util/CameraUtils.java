package de.virality.opticalframework.core.util;

public final class CameraUtils {

    private CameraUtils() { }

    /**
     * Camera parameters for GoPro action cams.
     * <p>
     * Those parameters are known to be valid for GoPro HERO 4 Black/Silver as stated
     * <a href="https://gopro.com/help/articles/Question_Answer/HERO4-Field-of-View-FOV-Information">here</a>
     * and also for the HERO 3+ Black edition as stated
     * <a href="https://de.gopro.com/support/articles/hero3-field-of-view-fov-information">here</a>. They may apply for
     * other models as well.
     */
    public enum GoProConfig {

        /** Wide FOV, 4x3 Resolution */
        W4x3(14, 94.4, 122.6, 149.2),
        /** Medium FOV, 4x3 Resolution */
        M4x3(21, 72.2, 94.4, 115.7),
        /** Narrow FOV, 4x3 Resolution */
        N4x3(28, 49.1, 64.6, 79.7),
        /** Wide FOV, 16x9 Resolution */
        W16x9(14, 69.5, 118.2, 133.6),
        /** Medium FOV, 16x9 Resolution */
        M16x9(21, 55.0, 94.4, 107.1),
        /** Narrow FOV, 16x9 Resolution */
        N16x9(28, 37.2, 64.4, 73.6);

        /** focal length in mm */
        public final double focalLength;
        /** vertical angle of view in degrees */
        public final double verticalFov;
        /** horizontal angle of view in degrees */
        public final double horizontalFov;
        /** diagonal angle of view in degrees */
        public final double diagonalFov;

        GoProConfig(double focalLength, double verticalFov, double horizontalFov, double diagonalFov) {
            this.focalLength = focalLength;
            this.verticalFov = verticalFov;
            this.horizontalFov = horizontalFov;
            this.diagonalFov = diagonalFov;
        }
    }
}
