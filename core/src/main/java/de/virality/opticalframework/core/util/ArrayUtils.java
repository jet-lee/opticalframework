package de.virality.opticalframework.core.util;

public final class ArrayUtils {

    private ArrayUtils() { }

    public static float[] doubleToFloatArray(double[] in) {
        float[] out = new float[in.length];
        for (int i = 0; i < in.length; i++) {
            out[i] = (float) in[i];
        }
        return out;
    }

    public static double[] floatToDoubleArray(float[] in) {
        double[] out = new double[in.length];
        for (int i = 0; i < in.length; i++) {
            out[i] = in[i];
        }
        return out;
    }

    public static double[] intToDoubleArray(int[] in) {
        double[] out = new double[in.length];
        for (int i = 0; i < in.length; i++) {
            out[i] = in[i];
        }
        return out;
    }

    /**
     * Shifts array values by one index to the left, thereby removing the first value, and adds a new value to the end.
     * @param array the array
     * @param value the new value which is placed at the last index position
     */
    public static void shiftAndInsert(double[] array, double value) {
        System.arraycopy(array, 1, array, 0, array.length - 1);
        array[array.length - 1] = value;
    }

    /**
     * Shifts array values by one index to the left, thereby removing the first value, and adds a new value to the end.
     * @param array the array
     * @param value the new value which is placed at the last index position
     */
    public static void shiftAndInsert(int[] array, int value) {
        System.arraycopy(array, 1, array, 0, array.length - 1);
        array[array.length - 1] = value;
    }
}
