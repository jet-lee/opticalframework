package de.virality.opticalframework.core.gui.drawer;

import de.virality.opticalframework.core.gui.ImageVisualization;

import java.awt.*;

/**
 * Common interface for everything that can draw on top of a GUI component.
 * <p>
 * Multiple drawers can be combined to construct complex visualizations.
 *
 * @see ImageVisualization
 */
public interface Drawer {

    /**
     * Draws the drawers contents on the supplied {@link Graphics2D} plane.
     * <p>
     * The scale denotes the current size of the graphics plane compared to its original size and may change e.g. when
     * the user is resizing the container window. It should always be taken into consideration when calculating
     * coordinates on the graphics, since they are usually supposed to scale together with the content. Other
     * calculations, like the size of a painted object, may or may not consider the current scale, depending on whether
     * they should scale with the content or not.
     *
     * @param graphics2D the {@link Graphics2D} to paint on
     * @param scaleX the current scale of the graphics plane on the x-axis
     * @param scaleY the current scale of the graphics plane on the y-axis
     */
    void draw(Graphics2D graphics2D, double scaleX, double scaleY);

}
