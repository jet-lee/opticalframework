package de.virality.opticalframework.core.util;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FileUtils {

    private FileUtils() { }

    /**
     * Finds all images inside the specified path.
     * <p>
     * It will return all images ending with ".png", ".jpg", ".jpeg" or their uppercase counterparts in the given
     * directory. It will not search the directory recursively.
     *
     * @param path the directory containing the images
     * @return a list of file names of all images that have been found in the directory
     * @throws IllegalArgumentException if the path does not point to a valid directory or read access is denied
     */
	public static List<String> readImagePath(String path) {
        final File dir = new File(path);
        final File[] files = dir.listFiles();
        if (files == null) {
            throw new IllegalArgumentException("Could not read " + path + "!" +
                    "Make sure it points to a valid directory and read access is granted.");
        } else {
            return Stream.of(files)
                    .filter(file -> !file.isDirectory())
                    .map(File::getName)
                    .filter(fileName -> fileName.toLowerCase().endsWith(".png")
                            || fileName.toLowerCase().endsWith(".jpg")
                            || fileName.toLowerCase().endsWith(".jpeg"))
                    .map(fileName -> path + File.separator + fileName)
                    .collect(Collectors.toList());
        }
	}
}
