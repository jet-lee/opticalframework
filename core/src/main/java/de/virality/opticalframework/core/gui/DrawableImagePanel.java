package de.virality.opticalframework.core.gui;

import de.virality.opticalframework.core.gui.drawer.Drawer;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * A {@link JPanel} that shows an image and optionally invokes additional {@link Drawer}s to draw on top of it.
 * <p>
 * {@link Drawer}s are added by calling {@link #addDrawable(Drawer)}.
 */
class DrawableImagePanel extends JPanel {

    private BufferedImage image;

    private List<Drawer> drawers = new ArrayList<>();

    public DrawableImagePanel(BufferedImage image) {
        super(true);
        this.image = image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public void addDrawable(Drawer drawer) {
        drawers.add(drawer);
    }

    public void removeDrawable(Drawer drawer) {
        drawers.remove(drawer);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        paintOnSize(g, getWidth(), getHeight());
    }

    private void paintOnSize(Graphics g, int canvasWidth, int canvasHeight) {
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // When window is resized, scale the image but keep the aspect ratio
        // http://www.codejava.net/java-se/graphics/drawing-an-image-with-automatic-scaling

        int imgWidth = image.getWidth(null);
        int imgHeight = image.getHeight(null);

        double imgAspect = (double) imgHeight / imgWidth;
        double canvasAspect = (double) canvasHeight / canvasWidth;

        int x1 = 0; // top left X position
        int y1 = 0; // top left Y position
        int x2; // bottom right X position
        int y2; // bottom right Y position

        if (canvasAspect > imgAspect) {
            y1 = canvasHeight;
            // keep image aspect ratio
            canvasHeight = (int) (canvasWidth * imgAspect);
            y1 = (y1 - canvasHeight) / 2;
        } else {
            x1 = canvasWidth;
            // keep image aspect ratio
            canvasWidth = (int) (canvasHeight / imgAspect);
            x1 = (x1 - canvasWidth) / 2;
        }
        x2 = canvasWidth + x1;
        y2 = canvasHeight + y1;

        g.drawImage(image, x1, y1, x2, y2, 0, 0, imgWidth, imgHeight, null);

        graphics2D.translate(x1, y1);

        double scaleX = (x2 - x1) / (double) imgWidth;
        double scaleY = (y2 - y1) / (double) imgHeight;

        for (Drawer d : drawers) {
            d.draw(graphics2D, scaleX, scaleY);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(image.getWidth(), image.getHeight());
    }

    public BufferedImage getContentAsImage() {
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(), image.getHeight(),
                BufferedImage.TYPE_INT_RGB);
        paintOnSize(bufferedImage.createGraphics(), image.getWidth(), image.getHeight());
        return bufferedImage;
    }
}
