package de.virality.opticalframework.core.matcher;

import de.virality.opticalframework.core.feature.FeaturePoint;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link FeaturePointMatcher} using the <em>Nearest Neighbor Ratio Matching</em> approach to find pairs of matching
 * {@link FeaturePoint}s.
 * <p>
 * For each point in the query set, the two nearest neighbors from the candidate set are retrieved with distances
 * <em>d1</em> and <em>d2</em>. If the ratio <em>d1/d2</em> of the distances is below a threshold value, the nearest
 * neighbor is considered a match. The threshold can be adjusted by using the corresponding constructor
 * {@link #SimpleFeaturePointMatcher(float)}.
 *
 * @see ParallelFeaturePointMatcher
 */
public class SimpleFeaturePointMatcher implements FeaturePointMatcher {

    public static final float DEFAULT_THRESHOLD = 0.65f;

    private final float threshold;

    public SimpleFeaturePointMatcher() {
        this(DEFAULT_THRESHOLD);
    }

    /**
     * @param threshold The threshold used for matching.
     *                  Lower values lead to more matches, but also more false positives
     */
    public SimpleFeaturePointMatcher(float threshold) {
        this.threshold = threshold;
    }

    @Override
    public <T extends FeaturePoint<T>> List<FeaturePointMatch<T>> findMatches(List<T> points1, List<T> points2) {
        List<FeaturePointMatch<T>> matches = new ArrayList<>();

        for (T p : points1) {
            FeaturePointMatch<T> match = findMatchForPoint(p, points2);
            if (match != null) {
                matches.add(match);
            }
        }

        return matches;
    }

    protected <T extends FeaturePoint<T>> FeaturePointMatch<T> findMatchForPoint(T p, List<T> candidates) {
        double dist, d1, d2;
        T match = null;
        d1 = d2 = Double.MAX_VALUE;

        for (T c : candidates) {
            dist = p.matchWith(c);

            if (dist < d1) {
                d2 = d1;
                d1 = dist;
                match = c;
            } else if (dist < d2) {
                d2 = dist;
            }
        }

        double score = d1 / d2;
        if (d1 / d2 < threshold && d2 != Double.MAX_VALUE) {
            return new FeaturePointMatch<>(p, match, score);
        } else {
            return null;
        }
    }
}
