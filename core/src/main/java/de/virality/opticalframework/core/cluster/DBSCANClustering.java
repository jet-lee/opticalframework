package de.virality.opticalframework.core.cluster;

import org.apache.commons.math3.ml.clustering.*;
import org.apache.commons.math3.ml.clustering.Clusterable;

/**
 * {@link Clustering} based on the DBSCAN algorithm by Martin Ester, Hans-Peter Kriegel, Jörg Sander and Xiaowei Xu.
 * <p>
 * This is a wrapper around Apache Commons {@link DBSCANClusterer}
 *
 * @see DBSCANClusterer
 */
public class DBSCANClustering extends ApacheCommonsClustererAdapter {

    private final double epsilon;
    private final int minPoints;

    public DBSCANClustering(double epsilon, int minPoints) {
        this.epsilon = epsilon;
        this.minPoints = minPoints;
    }

    @Override
    protected <T extends Clusterable> Clusterer<T> createClusterer() {
        return new DBSCANClusterer<>(epsilon, minPoints);
    }
}
