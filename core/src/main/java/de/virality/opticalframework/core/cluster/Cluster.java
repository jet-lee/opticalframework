package de.virality.opticalframework.core.cluster;

import java.util.Arrays;

/**
 * Represents a cluster of data points. Usually a set of clusters is obtained by performing a {@link Clustering}.
 */
public class Cluster {

    private int size;

    private int index;

    private double[] center;

    public Cluster(int size, int index, double[] center) {
        this.size = size;
        this.index = index;
        this.center = center;
    }

    public int getSize() {
        return size;
    }

    public int getIndex() {
        return index;
    }

    public double[] getCenter() {
        return center;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cluster cluster = (Cluster) o;

        if (size != cluster.size) return false;
        if (index != cluster.index) return false;
        return Arrays.equals(center, cluster.center);

    }

    @Override
    public int hashCode() {
        int result = size;
        result = 31 * result + index;
        result = 31 * result + Arrays.hashCode(center);
        return result;
    }

    @Override
    public String toString() {
        return "Cluster{" +
                "size=" + size +
                ", index=" + index +
                ", center=" + Arrays.toString(center) +
                '}';
    }
}
