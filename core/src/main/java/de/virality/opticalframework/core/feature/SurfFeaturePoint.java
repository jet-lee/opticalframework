package de.virality.opticalframework.core.feature;

import de.virality.opticalframework.core.cluster.Clusterable;
import de.virality.opticalframework.core.util.MathUtils;

import java.util.Arrays;

/**
 * Class representing a SURF feature point detected by a {@link Surf} feature extractor.
 */
public class SurfFeaturePoint extends FeaturePoint<SurfFeaturePoint> implements Clusterable {

    private final double scale;
    private final double orientation;
    private final int laplacian;
    private final double[] descriptor;

    public SurfFeaturePoint(double x, double y, double scale, double orientation, int laplacian, double[] descriptor) {
        super(x, y);
        this.scale = scale;
        this.orientation = orientation;
        this.laplacian = laplacian;
        this.descriptor = descriptor;
    }

    public double getScale() {
        return scale;
    }

    public double getOrientation() {
        return orientation;
    }

    public int getLaplacian() {
        return laplacian;
    }

    public double[] getDescriptor() {
        return descriptor;
    }

    /**
     * Gets the distance in descriptor space between two SURF points.
     * <p>
     * This can be used as a similarity score to match SURF points against each other.
     * Smaller values indicate higher similarity.
     *
     * @param another The SURF point to compare to
     * @return The euclidean distance between the two interest point descriptors
     */
    @Override
    public double matchWith(SurfFeaturePoint another) {
        return MathUtils.euclideanDistance(descriptor, another.descriptor);
    }

    @Override
    public double[] getPoint() {
        return descriptor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SurfFeaturePoint that = (SurfFeaturePoint) o;

        if (Double.compare(that.x, x) != 0) return false;
        if (Double.compare(that.y, y) != 0) return false;
        if (Double.compare(that.scale, scale) != 0) return false;
        if (Double.compare(that.orientation, orientation) != 0) return false;
        if (laplacian != that.laplacian) return false;
        return Arrays.equals(descriptor, that.descriptor);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(scale);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(orientation);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + laplacian;
        result = 31 * result + Arrays.hashCode(descriptor);
        return result;
    }

    @Override
    public String toString() {
        return "FeaturePoint{" +
                "x=" + x +
                ", y=" + y +
                ", scale=" + scale +
                ", orientation=" + orientation +
                ", laplacian=" + laplacian +
                ", descriptor=" + Arrays.toString(descriptor) +
                '}';
    }
}
