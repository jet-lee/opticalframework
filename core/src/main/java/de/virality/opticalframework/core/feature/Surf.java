package de.virality.opticalframework.core.feature;

import org.opencv.core.Mat;

import java.util.Arrays;
import java.util.List;

/**
 * A feature extractor using the SURF (Speeded Up Robust Features) algorithm by Herbert Bay, Tinne Tuytelaars and Luc
 * Van Gool.
 * <p>
 * This class is just a wrapper around the C++ OpenSURF library by Chris Evans. It calls native C++ code and therefore
 * each client using this class needs to load the corresponding native library:
 *
 * <pre>
 *     static {
 *         System.loadLibrary(Surf.NATIVE_LIBRARY_NAME);
 *     }
 * </pre>
 */
public class Surf implements FeaturePointExtractor<SurfFeaturePoint> {

    public static final String NATIVE_LIBRARY_NAME = "opensurf";

    public static final int DESCRIPTOR_LENGTH = 64;

    public static final int DEFAULT_OCTAVES = 5;

    public static final int DEFAULT_INTERVALS = 4;

    public static final int DEFAULT_INIT_SAMPLE = 2;

    public static final float DEFAULT_THRESHOLD = 0.0004f;

    private boolean upright;

    private int octaves;

    private int intervals;

    private int initSample;

    private float threshold;

    public Surf() {
        this(false, DEFAULT_OCTAVES, DEFAULT_INTERVALS, DEFAULT_INIT_SAMPLE, DEFAULT_THRESHOLD);
    }

    public Surf(boolean upright, int octaves, int intervals, int initSample, float threshold) {
        this.upright = upright;
        this.octaves = octaves;
        this.intervals = intervals;
        this.initSample = initSample;
        this.threshold = threshold;
    }

    public boolean isUpright() {
        return upright;
    }

    public void setUpright(boolean upright) {
        this.upright = upright;
    }

    public int getOctaves() {
        return octaves;
    }

    public void setOctaves(int octaves) {
        this.octaves = octaves;
    }

    public int getIntervals() {
        return intervals;
    }

    public void setIntervals(int intervals) {
        this.intervals = intervals;
    }

    public int getInitSample() {
        return initSample;
    }

    public void setInitSample(int initSample) {
        this.initSample = initSample;
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    @Override
    public List<SurfFeaturePoint> extractFeaturePoints(Mat img) {
        return Arrays.asList(detectDescribe(img.getNativeObjAddr(), upright, octaves, intervals, initSample, threshold));
    }

    private native SurfFeaturePoint[] detectDescribe(long nativeImageAddress,
                                                     boolean upright,
                                                     int octaves,
                                                     int intervals,
                                                     int initSample,
                                                     float threshold);
}
