package de.virality.opticalframework.core.gui.drawer;

import de.virality.opticalframework.core.feature.FeaturePoint;

import java.awt.*;

/**
 * A default {@link FeaturePointDrawer.Style} that draws a {@link FeaturePoint} as a cross with the center being the
 * point coordinates.
 *
 * @param <T> the feature point type
 */
public class SingleColorStyle<T extends FeaturePoint> implements FeaturePointDrawer.Style<T> {

    private static final int CROSS_RADIUS = 4;

    private static final int STROKE_WIDTH = 1;

    private final Stroke stroke = new BasicStroke(STROKE_WIDTH);

    private final Paint paint;

    public SingleColorStyle(Color color) {
        this.paint = color;
    }

    @Override
    public void onDrawFeature(Graphics2D graphics2D, FeaturePoint featurePoint, double scaleX, double scaleY) {
        graphics2D.setPaint(paint);
        graphics2D.setStroke(stroke);

        int x = (int) (featurePoint.getX() * scaleX);
        int y = (int) (featurePoint.getY() * scaleY);

        graphics2D.drawLine(x - CROSS_RADIUS, y, x + CROSS_RADIUS, y);
        graphics2D.drawLine(x, y - CROSS_RADIUS, x, y + CROSS_RADIUS);
    }
}
