package de.virality.opticalframework.core.gui;

import de.virality.opticalframework.core.gui.drawer.Drawer;
import de.virality.opticalframework.core.util.ImageUtils;
import org.opencv.core.Mat;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * A {@link Visualization} that shows an image and optionally invokes additional {@link Drawer}s to draw on top of it.
 * <p>
 * {@link Drawer}s are added by calling {@link #addDrawer(Drawer)}. The image can be updated, e.g. to show frames of a
 * video stream, see the various {@code setImage()} variants.
 * The current content, including image and drawers, can also be saved to a file, see {@link #save(String)}.
 */
public class ImageVisualization extends Visualization {

    protected final DrawableImagePanel imagePanel;

    public ImageVisualization(String imagePath) {
        this(ImageUtils.loadImageFromPath(imagePath));
    }

    public ImageVisualization(Mat img) {
        this(ImageUtils.matToBufferedImage(img));
    }

    public ImageVisualization(BufferedImage img) {
        this.imagePanel = new DrawableImagePanel(img);
    }

    /**
     * Update the displayed image.
     *
     * @param imagePath path to the new image
     * @see #setImage(BufferedImage)
     * @see #setImage(Mat)
     */
    public void setImage(String imagePath) {
        setImage(ImageUtils.loadImageFromPath(imagePath));
    }

    /**
     * Update the displayed image.
     *
     * @param img path to the new image
     * @see #setImage(BufferedImage)
     * @see #setImage(String)
     */
    public void setImage(Mat img) {
        setImage(ImageUtils.matToBufferedImage(img));
    }

    /**
     * Update the displayed image.
     * The new image will replace the currently shown image immediately, no need to call {@link #refresh()}.
     * Images can be updated continuously, e.g. to show a stream of video frames.
     *
     * @param img path to the new image
     * @see #setImage(Mat)
     * @see #setImage(String)
     */
    public void setImage(BufferedImage img) {
        imagePanel.setImage(img);
        refresh();
    }

    /**
     * Adds a layer to the image that will be drawn on top of it.
     * You can add multiple {@link Drawer}s to an image to achieve complex visualizations. {@link Drawer}s are drawn in
     * the same order as they were added.
     *
     * @param drawer a drawer that draws on top of the image
     */
    public void addDrawer(Drawer drawer) {
        imagePanel.addDrawable(drawer);
        refresh();
    }

    /**
     * Remove a previously added {@link Drawer} from the visualization.
     * @param drawer the drawer to remove
     */
    public void removeDrawer(Drawer drawer) {
        imagePanel.removeDrawable(drawer);
        refresh();
    }

    @Override
    protected JComponent getComponent() {
        return imagePanel;
    }

    /**
     * Save the current content of the visualization to a file, including the image and all {@link Drawer}s that are
     * currently added.
     *
     * @param fileName the file path
     * @throws IOException when something goes wrong while saving
     */
    public void save(String fileName) throws IOException {
        BufferedImage content = imagePanel.getContentAsImage();
        ImageIO.write(content, "png", new File(fileName));
    }
}
