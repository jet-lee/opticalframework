package de.virality.opticalframework.core.cluster;

import de.virality.opticalframework.core.feature.FeaturePoint;

/**
 * An interface aimed at simplifying clustering of {@link FeaturePoint}s or arbitrary data points in general.
 */
public interface Clusterable {

    /**
     * @return The data point used for clustering
     */
    double[] getPoint();
}
