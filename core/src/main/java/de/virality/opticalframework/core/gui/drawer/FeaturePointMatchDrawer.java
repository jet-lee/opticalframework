package de.virality.opticalframework.core.gui.drawer;

import de.virality.opticalframework.core.feature.FeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;

import java.awt.*;
import java.util.List;

/**
 * A {@link Drawer} to visualize the movements of matched {@link FeaturePoint}s.
 * <p>
 * You can pass a list of {@link FeaturePointMatch}es, which will be drawn with the first element in green and second
 * element in red, connected by a line in magenta.
 *
 * @param <T> the feature point type
 */
public class FeaturePointMatchDrawer<T extends FeaturePoint> implements Drawer {

    private Paint paint = Color.MAGENTA;

    private Stroke stroke = new BasicStroke(1);

    private List<FeaturePointMatch<T>> matches;

    private FeaturePointDrawer.Style<T> firstDrawer = new SingleColorStyle<>(Color.GREEN);

    private FeaturePointDrawer.Style<T> secondDrawer = new SingleColorStyle<>(Color.RED);

    public void setMatches(List<FeaturePointMatch<T>> matches) {
        this.matches = matches;
    }

    @Override
    public void draw(Graphics2D graphics2D, double scaleX, double scaleY) {
        if (matches == null) {
            return;
        }

        for (FeaturePointMatch<T> match : matches) {
            secondDrawer.onDrawFeature(graphics2D, match.second, scaleX, scaleY);
            firstDrawer.onDrawFeature(graphics2D, match.first, scaleX, scaleY);

            graphics2D.setPaint(paint);
            graphics2D.setStroke(stroke);
            graphics2D.drawLine((int) (match.first.getX() * scaleX), (int) (match.first.getY() * scaleY),
                    (int) (match.second.getX() * scaleX), (int) (match.second.getY() * scaleY));
        }
    }
}
