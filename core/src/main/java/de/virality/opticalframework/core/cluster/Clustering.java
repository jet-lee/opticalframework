package de.virality.opticalframework.core.cluster;

import java.util.List;

/**
 * Common interface for different clustering algorithms.
 */
public interface Clustering {

    /**
     * Calculates clusters from the given input points.
     * The algorithm used for clustering depends on the implementation.
     *
     * @param points a collection of {@link Clusterable} input points
     * @return the {@link Cluster}s as determined by the clustering algorithm
     */
    List<Cluster> calculateClusters(List<? extends Clusterable> points);
}
