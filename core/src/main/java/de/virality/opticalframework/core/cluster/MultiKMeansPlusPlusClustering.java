package de.virality.opticalframework.core.cluster;

import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.Clusterer;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.ml.clustering.MultiKMeansPlusPlusClusterer;

/**
 * An extension to {@link KMeansPlusPlusClustering} that performs multiple clusterings and chooses the best result.
 * <p>
 * This is a wrapper around Apache Commons {@link MultiKMeansPlusPlusClusterer}.
 *
 * @see MultiKMeansPlusPlusClusterer
 */
public class MultiKMeansPlusPlusClustering extends KMeansPlusPlusClustering {

    public static final int DEFAULT_NUM_TRIALS = 3;

    private int numTrials;

    public MultiKMeansPlusPlusClustering() {
        this(DEFAULT_K, DEFAULT_MAX_ITERATIONS, DEFAULT_NUM_TRIALS);
    }

    public MultiKMeansPlusPlusClustering(int k, int maxIterations, int numTrials) {
        super(k, maxIterations);
        this.numTrials = numTrials;
    }

    @Override
    protected <T extends Clusterable> Clusterer<T> createClusterer() {
        @SuppressWarnings("unchecked")
        KMeansPlusPlusClusterer<T> kmeans = (KMeansPlusPlusClusterer<T>) super.createClusterer();
        return new MultiKMeansPlusPlusClusterer<>(kmeans, numTrials);
    }
}
