#include <opencv/cv.h>

#include "de_virality_opticalframework_core_feature_Surf.h"
#include "openSurf/ipoint.h"
#include "openSurf/surflib.h"

JNIEXPORT jobjectArray JNICALL Java_de_virality_opticalframework_core_feature_Surf_detectDescribe
    (JNIEnv *env, jobject obj, jlong imgMatAddr, jboolean upright, jint octaves, jint intervals, jint initSample, jfloat thres)
{
    cv::Mat* imageMat = (cv::Mat*)imgMatAddr;
    IplImage img = *imageMat;
    IpVec ipts;

    surfDetDes(&img, ipts, upright, octaves, intervals, initSample, thres);

    jclass iPointClass = env->FindClass("de/virality/opticalframework/core/feature/SurfFeaturePoint");
    if (iPointClass == NULL) return NULL;

    jmethodID constructor = env->GetMethodID(iPointClass, "<init>", "(DDDDI[D)V");

    jobjectArray result = env->NewObjectArray(ipts.size(), iPointClass, NULL);
    if (result == NULL) return NULL;

    for (int i = 0; i < ipts.size(); i++) {
        Ipoint ipoint = ipts[i];

        jdouble fill[64];
        for (int j = 0; j < 64; j++) {
            fill[j] = ipoint.descriptor[j];
        }

        jdoubleArray descriptor = env->NewDoubleArray(64);
        env->SetDoubleArrayRegion(descriptor, 0, 64, fill);

        jobject javaInterestPoint = env->NewObject(iPointClass, constructor,
                                                   ipoint.x,
                                                   ipoint.y,
                                                   ipoint.scale,
                                                   ipoint.orientation,
                                                   ipoint.laplacian,
                                                   descriptor);
        env->SetObjectArrayElement(result, i, javaInterestPoint);
    }

    return result;
}
