package de.virality.opticalframework.core.feature;

import org.junit.Before;
import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SurfTest {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        System.loadLibrary(Surf.NATIVE_LIBRARY_NAME);
    }

    private List<SurfFeaturePoint> points;

    @Before
    public void getSurfPoints() {
        Mat img = Highgui.imread(getClass().getResource("/whitehouse.jpg").getPath());
        Surf surf = new Surf();
        points = surf.extractFeaturePoints(img);
    }

    @Test
    public void testSurfDetection() {
        assertTrue("should detect interest points", points.size() > 0);
    }

    @Test
    public void testSurfDescription() {
        SurfFeaturePoint p1 = points.get(0);
        SurfFeaturePoint p2 = points.get(1);
        double[] emptyArray = new double[Surf.DESCRIPTOR_LENGTH];

        assertFalse("descriptor should not be empty", Arrays.equals(emptyArray, p1.getDescriptor()));
        assertFalse("two different points should have different descriptors",
                Arrays.equals(p1.getDescriptor(), p2.getDescriptor()));
    }

    @Test
    public void testFeaturePointDistance() {
        SurfFeaturePoint p1 = points.get(0);
        SurfFeaturePoint p2 = points.get(1);

        assertTrue("two different points should have distance > 0", p1.matchWith(p2) > 0);
        assertTrue("two equal points should have distance 0", p1.matchWith(p1) == 0);
    }
}
