package de.virality.opticalframework.odometry.outlier;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.util.MathUtils;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.util.ArrayList;
import java.util.List;

/**
 * This {@link OutlierDetection} calculates the mean distance between matched point pairs and removes all matches with
 * a distance greater than the mean multiplied by a given outlier factor.
 */
public class SimpleMeanOutlierDetection implements OutlierDetection {

    private double outlierFactor;

    public SimpleMeanOutlierDetection(double outlierFactor) {
        this.outlierFactor = outlierFactor;
    }

    @Override
    public List<FeaturePointMatch<SurfFeaturePoint>> removeOutliers(List<FeaturePointMatch<SurfFeaturePoint>> input) {
        double[] movements = new double[input.size()];
        for (int i = 0; i < input.size(); i++) {
            SurfFeaturePoint p1 = input.get(i).first;
            SurfFeaturePoint p2 = input.get(i).second;

            movements[i] = MathUtils.euclideanDistance(new double[]{p1.getX(), p1.getY()},
                                                       new double[]{p2.getX(), p2.getY()});
        }

        double mean = new Mean().evaluate(movements);

        List<FeaturePointMatch<SurfFeaturePoint>> output = new ArrayList<>();
        for (int i = 0; i < movements.length; i++) {
            if (movements[i] < mean * outlierFactor) {
                output.add(input.get(i));
            }
        }

        return output;
    }
}
