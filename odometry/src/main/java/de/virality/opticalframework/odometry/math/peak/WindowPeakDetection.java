package de.virality.opticalframework.odometry.math.peak;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link PeakDetection} algorithm using a window and an optional threshold and plausibility check.
 * <p>
 * A data point is considered a maximum (minimum), if there is no other value inside the current window equal or
 * greater (less) than this data point. The window is symmetric around the data point.
 * <p>
 * A threshold can be applied, requiring the maximum to have an absolute value greater or equal to the threshold.
 * Additionally, a so called plausibility check can be applied, requiring a minimum to have a negative value and a
 * maximum to have a positive value respectively.
 */
public class WindowPeakDetection implements PeakDetection {

    private final int windowSize;

    private final double threshold;

    private final boolean withPlausibleCheck;

    private final double[] data;

    private List<Integer> minima = new ArrayList<>();

    private List<Integer> maxima = new ArrayList<>();

    private int count = 0;

    public WindowPeakDetection(int windowSize) {
        this(windowSize, 0, false);
    }

    public WindowPeakDetection(int windowSize, double threshold, boolean withPlausibleCheck) {
        this.windowSize = windowSize;
        this.threshold = threshold;
        this.withPlausibleCheck = withPlausibleCheck;
        this.data = new double[windowSize];
    }

    @Override
    public boolean addValue(double value) {
        System.arraycopy(data, 1, data, 0, data.length - 1);
        data[data.length - 1] = value;
        count++;
        return findPeak();
    }

    @Override
    public List<Integer> getMaxima() {
        return maxima;
    }

    @Override
    public List<Integer> getMinima() {
        return minima;
    }

    private boolean findPeak() {
        if (count < windowSize) {
            return false;
        }

        double candidateValue = data[windowSize / 2];

        if (Math.abs(candidateValue) < threshold) {
            return false;
        }

        boolean isMaximum = true;
        boolean isMinimum = true;
        for (int i = 0; i < windowSize; i++) {
            if (data[i] > candidateValue) {
                isMaximum = false;
            } else if (data[i] < candidateValue) {
                isMinimum = false;
            }
        }

        if (withPlausibleCheck) {
            if ((isMaximum && candidateValue < 0) || (isMinimum && candidateValue > 0)) {
                return false;
            }
        }

        if (isMaximum) {
            maxima.add(count - windowSize / 2 - windowSize % 2);
        } else if (isMinimum) {
            minima.add(count - windowSize / 2 - windowSize % 2);
        }

        return isMaximum || isMinimum;
    }
}
