package de.virality.opticalframework.odometry.math.peak;

import java.util.List;

/**
 * Common interface for peak detection algorithms.
 * <p>
 * Since data is analysed live as it comes in and some form of a processing window is required, peaks can only be
 * reported with a delay.
 */
public interface PeakDetection {

    /**
     * Adds a new data point and returns {@code true} if a new peak was detected.
     * <p>
     * Keep in mind, that due to processing delays, the new peak, if found, is usually not the value that was just
     * added. To get the list of indices where peaks have been found, call {@link #getMaxima()} or {@link #getMinima()}.
     *
     * @param value A new data point
     * @return True if a new peak was detected, false otherwise
     */
    boolean addValue(double value);

    /**
     * @return A list of indices, where maxima have been found
     */
    List<Integer> getMaxima();

    /**
     * @return A list of indices, where minima have been found
     */
    List<Integer> getMinima();
}
