package de.virality.opticalframework.odometry.math.filter;

import de.virality.opticalframework.core.util.ArrayUtils;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.LUDecomposition;

import static java.lang.Math.pow;

/**
 * Savitzky-Golay {@link Filter} implementation.
 * <p>
 * Based on:
 * <a href="https://github.com/RaineForest/ECG-Viewer/blob/master/sgfilter/src/mr/go/sgfilter/SGFilter.java">
 *     https://github.com/RaineForest/ECG-Viewer/blob/master/sgfilter/src/mr/go/sgfilter/SGFilter.java</a>
 */
public class SGFilter implements Filter {

    private final double[] coeffs;

    private final double[] data;


    /**
     * Constructs a Savitzky-Golay filter which uses the specified number of surrounding data points and polynomial
     * degree
     */
    public SGFilter(int windowSize, int degree) {
        if (windowSize < 0 || windowSize % 2 == 0) {
            throw new IllegalArgumentException("windowSize must be >0 and must be an odd number");
        }

        this.data = new double[windowSize];
        int spaceSides = (windowSize - 1) / 2;
        this.coeffs = computeSGCoefficients(spaceSides, spaceSides, degree);
    }

    /**
     * Computes the Savitzky-Golay coefficients for the given parameters.
     *
     * @param nl number of past data points filter will use
     * @param nr number of future data points filter will use
     * @param degree order of smoothing polynomial
     * @return Savitzky-Golay coefficients
     * @throws IllegalArgumentException if {@code nl < 0} or {@code nr < 0} or {@code nl + nr < degree}
     */
    private static double[] computeSGCoefficients(int nl, int nr, int degree) {
        if (nl < 0 || nr < 0 || nl + nr < degree)
            throw new IllegalArgumentException("Bad arguments");
        Array2DRowRealMatrix matrix = new Array2DRowRealMatrix(degree + 1, degree + 1);
        double[][] a = matrix.getDataRef();
        double sum;
        for (int i = 0; i <= degree; i++) {
            for (int j = 0; j <= degree; j++) {
                sum = (i == 0 && j == 0) ? 1 : 0;
                for (int k = 1; k <= nr; k++)
                    sum += pow(k, i + j);
                for (int k = 1; k <= nl; k++)
                    sum += pow(-k, i + j);
                a[i][j] = sum;
            }
        }
        double[] b = new double[degree + 1];
        b[0] = 1;
        ArrayRealVector b1 = new ArrayRealVector(b);
        b1 = (ArrayRealVector) (new LUDecomposition(matrix)).getSolver().solve(b1);
        b = b1.toArray();
        double[] coeffs = new double[nl + nr + 1];
        for (int n = -nl; n <= nr; n++) {
            sum = b[0];
            for (int m = 1; m <= degree; m++)
                sum += b[m] * pow(n, m);
            coeffs[n + nl] = sum;
        }
        return coeffs;
    }

    @Override
    public double filter(double value) {
        ArrayUtils.shiftAndInsert(data, value);

        float sum = 0;
        for (int i = 0; i < data.length; i++) {
            sum += data[i] * coeffs[i];
        }

        return sum;
    }
}
