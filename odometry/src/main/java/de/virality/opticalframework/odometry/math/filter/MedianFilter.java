package de.virality.opticalframework.odometry.math.filter;

import org.apache.commons.math3.stat.descriptive.rank.Median;

/**
 * A simple {@link Filter} implementation that calculates the median value over a sliding window to smooth the data.
 */
public class MedianFilter implements Filter {

    private final int windowSize;

    private final double[] data;

    private final Median median = new Median();

    private int count = 0;

    public MedianFilter(int windowSize) {
        this.windowSize = windowSize;
        this.data = new double[windowSize];
    }

    @Override
    public double filter(double value) {
        data[count % windowSize] = value;
        count++;
        return median.evaluate(data, 0, Math.min(count, windowSize));
    }
}
