package de.virality.opticalframework.odometry.math.filter;

import org.apache.commons.math3.stat.descriptive.moment.Mean;

/**
 * A simple {@link Filter} implementation that calculates the mean value over a window to smooth the data.
 */
public class MeanFilter implements Filter {

    private final int windowSize;

    private final double[] data;

    private final Mean mean = new Mean();

    private int count = 0;

    public MeanFilter(int windowSize) {
        this.windowSize = windowSize;
        this.data = new double[windowSize];
    }

    @Override
    public double filter(double value) {
        data[count % windowSize] = value;
        count++;
        return mean.evaluate(data, 0, Math.min(count, windowSize));
    }
}
