package de.virality.opticalframework.odometry.outlier;

import de.virality.opticalframework.core.annotations.Experimental;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.util.MathUtils;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import java.util.ArrayList;
import java.util.List;

/**
 * This {@link OutlierDetection} calculates a given quantile of the distances of the matched point pairs and removes all
 * pairs with greater distance than this quantile.
 */
@Experimental("usefulness not tested yet")
public class PercentileOutlierDetection implements OutlierDetection {

    private double quantile;

    public PercentileOutlierDetection(double quantile) {
        this.quantile = quantile;
    }

    @Override
    public List<FeaturePointMatch<SurfFeaturePoint>> removeOutliers(List<FeaturePointMatch<SurfFeaturePoint>> input) {
        double[] movements = new double[input.size()];
        for (int i = 0; i < input.size(); i++) {
            SurfFeaturePoint p1 = input.get(i).first;
            SurfFeaturePoint p2 = input.get(i).second;

            movements[i] = MathUtils.euclideanDistance(new double[]{p1.getX(), p1.getY()},
                                                       new double[]{p2.getX(), p2.getY()});
        }

        double percentile = new Percentile(quantile).evaluate(movements);

        List<FeaturePointMatch<SurfFeaturePoint>> output = new ArrayList<>();
        for (int i = 0; i < movements.length; i++) {
            if (movements[i] < percentile) {
                output.add(input.get(i));
            }
        }

        return output;
    }
}
