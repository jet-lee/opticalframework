package de.virality.opticalframework.odometry.pedometer;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.odometry.math.filter.Filter;
import de.virality.opticalframework.odometry.math.peak.PeakDetection;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for {@link Pedometer}s producing a data stream which is smoothed by a {@link Filter} and then analyzed by
 * a {@link PeakDetection} to detect steps.
 */
public abstract class BaseSurfPedometer implements Pedometer {

    private final Filter filter;

    private final PeakDetection peakDetection;

    private final List<Double> filteredData = new ArrayList<>();

    private final List<Double> rawData = new ArrayList<>();

    private int stepCount;

    public BaseSurfPedometer(Filter filter, PeakDetection peakDetection) {
        this.filter = filter;
        this.peakDetection = peakDetection;
    }

    public void feed(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        double rawValue = getFrameRawValue(matches);
        rawData.add(rawValue);

        double filteredValue = filter.filter(rawValue);
        filteredData.add(filteredValue);

        if (peakDetection.addValue(filteredValue)) {
            stepCount++;
        }
    }

    protected abstract double getFrameRawValue(List<FeaturePointMatch<SurfFeaturePoint>> matches);

    @Override
    public int getStepCount() {
        return stepCount;
    }

    public List<Double> getFilteredData() {
        return filteredData;
    }

    public List<Double> getRawData() {
        return rawData;
    }

    public Filter getFilter() {
        return filter;
    }

    public PeakDetection getPeakDetection() {
        return peakDetection;
    }
}
