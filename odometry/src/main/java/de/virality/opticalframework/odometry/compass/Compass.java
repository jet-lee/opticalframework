package de.virality.opticalframework.odometry.compass;

/**
 * Common interface for components providing a heading estimation.
 */
public interface Compass {

    /**
     * @return the current heading estimation
     */
    double getHeading();
}
