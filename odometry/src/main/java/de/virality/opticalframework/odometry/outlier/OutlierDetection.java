package de.virality.opticalframework.odometry.outlier;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;

import java.util.List;

/**
 * Common interface for outlier detection algorithms to remove false matches from a collection of
 * {@link SurfFeaturePoint} matches.
 */
public interface OutlierDetection {

    /**
     * @param input a list of {@link FeaturePointMatch}es of {@link SurfFeaturePoint}
     * @return a new list containing the input matches, but with outliers removed
     */
    List<FeaturePointMatch<SurfFeaturePoint>> removeOutliers(List<FeaturePointMatch<SurfFeaturePoint>> input);
}
