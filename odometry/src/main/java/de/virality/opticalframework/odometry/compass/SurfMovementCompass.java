package de.virality.opticalframework.odometry.compass;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import org.apache.commons.math3.stat.descriptive.rank.Median;

import java.util.List;

/**
 * A {@link Compass} implementation that estimates the camera heading by analyzing the horizontal distance of
 * {@link SurfFeaturePoint} pairs that have been matched between different images/frames.
 * <p>
 * The heading change from one frame to another is determined by calculating the median of the horizontal movement of
 * all {@link SurfFeaturePoint} matches found between these frames. The median is then divided by a value denoting the
 * horizontal amount of pixels per degree, which is dependant on the video resolution and the camera field of view.
 * <p>
 * Whenever there are no feature point matches found between two frames, the last known heading change is applied.
 * <p>
 * The concept was presented in: <br/>
 * <em>"Visual Odometry for pedestrians using scale and orientation features of SURF-Points"</em>, Master thesis by
 * Robert Gutschale (2016) at LMU München, Informatik, Lehrstuhl für Mobile Und Verteilte Systeme
 */
public class SurfMovementCompass implements Compass {

    private final Median median = new Median();

    private double heading = 0;
    private double pixelPerDegree;
    private double lastHeadingChange = 0;

    /**
     * Constructs a new compass instance using the specified parameters to calculate the horizontal pixels per degree.
     *
     * @param horizontalResolution the horizontal resolution of the input video stream
     * @param fieldOfView the field of view of the video camera
     * @see #SurfMovementCompass(double)
     */
    public SurfMovementCompass(int horizontalResolution, double fieldOfView) {
        this(horizontalResolution / fieldOfView);
    }

    /**
     * @param pixelPerDegree the amount of horizontal pixels per degree of movement
     * @see #SurfMovementCompass(int, double)
     */
    public SurfMovementCompass(double pixelPerDegree) {
        this.pixelPerDegree = pixelPerDegree;
    }

    /**
     * Update the current heading estimation by processing the provided {@link FeaturePointMatch}es.
     * The new heading can be retrieved by calling {@link #getHeading()}.
     *
     * @param matches a list of {@link FeaturePointMatch}es
     */
    public void feed(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        double headingChange;

        if (matches == null || matches.isEmpty()) {
            headingChange = lastHeadingChange;
        } else {
            double avgXMovement = getMedianXMovementDelta(matches);
            headingChange = avgXMovement / pixelPerDegree;
        }

        heading += headingChange;
        heading %= 360;
        if (heading < 0) {
            heading += 360;
        }
        lastHeadingChange = headingChange;
    }

    @Override
    public double getHeading() {
        return heading;
    }

    public void setHeading(double heading) {
        this.heading = heading;
    }

    public void setPixelPerDegree(double pixelPerDegree) {
        this.pixelPerDegree = pixelPerDegree;
    }

    public double getPixelPerDegree() {
        return pixelPerDegree;
    }

    private double getMedianXMovementDelta(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        double[] values = new double[matches.size()];
        for (int i = 0; i < matches.size(); i++) {
            FeaturePointMatch<SurfFeaturePoint> match = matches.get(i);
            SurfFeaturePoint p1 = match.first;
            SurfFeaturePoint p2 = match.second;

            values[i] = p1.getX() - p2.getX();
        }

        return median.evaluate(values);
    }
}
