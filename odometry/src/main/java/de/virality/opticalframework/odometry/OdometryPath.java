package de.virality.opticalframework.odometry;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a path constructed by an odometry system.
 * <p>
 * Each path is described by a series of coordinates. A new coordinate can either be added directly or by adding a step
 * with a corresponding heading and step size.
 */
public class OdometryPath {

    private double stepLength;
    private List<Point2D> path = new ArrayList<>();

    /**
     * Begin a new path.
     *
     * @param startX the x coordinate of the path starting point
     * @param startY the y coordinate of the path starting point
     * @param stepLength the step length used to calculate new positions when steps are added
     */
    public OdometryPath(double startX, double startY, double stepLength) {
        this(new Point2D.Double(startX, startY), stepLength);
    }

    /**
     * Begin a new path.
     *
     * @param startPoint the starting point of the path
     * @param stepLength the step length used to calculate new positions when steps are added
     */
    public OdometryPath(Point2D startPoint, double stepLength) {
        this.stepLength = stepLength;
        this.path.add(startPoint);
    }

    public Point2D getStartPoint() {
        return path.get(0);
    }

    public double getStepLength() {
        return stepLength;
    }

    public void setStepLength(double stepLength) {
        this.stepLength = stepLength;
    }

    /**
     * @return the current path position, meaning the last element of the path coordinates
     */
    public Point2D getCurrentPathPosition() {
        return path.get(path.size() - 1);
    }

    /**
     * @return the list of coordinates representing the path
     */
    public List<Point2D> getWayPoints() {
        return path;
    }

    /**
     * Append a new position to the path by adding a new step. The new position is calculated for the given heading and
     * the step length specified at path construction.
     *
     * @param heading the heading in which the step was taken
     */
    public void appendStep(double heading) {
        Point2D lastPosition = getCurrentPathPosition();

        double newPosX = lastPosition.getX() + Math.sin(Math.toRadians(heading)) * stepLength;
        double newPosY = lastPosition.getY() - Math.cos(Math.toRadians(heading)) * stepLength;

        path.add(new Point2D.Double(newPosX, newPosY));
    }

    /**
     * @see #appendPosition(Point2D)
     */
    public void appendPosition(double posX, double posY) {
        appendPosition(new Point2D.Double(posX, posY));
    }

    /**
     * Append a position to the path.
     *
     * @param position the new position coordinates
     * @see #updatePosition(Point2D)
     */
    public void appendPosition(Point2D position) {
        path.add(position);
    }

    /**
     * @see #updatePosition(Point2D)
     */
    public void updatePosition(double posX, double posY) {
        appendPosition(new Point2D.Double(posX, posY));
    }

    /**
     * Update the current path position.
     *
     * In contrast to {@link #appendPosition(Point2D)}, no new position is added, instead the last path position is
     * replaced by the new one.
     *
     * @param position the new position coordinates
     */
    public void updatePosition(Point2D position) {
        path.set(path.size() - 1, position);
    }
}
