package de.virality.opticalframework.odometry.outlier;

import de.virality.opticalframework.core.annotations.Experimental;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.util.ArrayList;
import java.util.List;

/**
 * This {@link OutlierDetection} calculates the mean angle of direction of the vectors between matched point pairs and
 * removes all pairs, which deviate from this mean by more than a given angle.
 */
@Experimental("Untested")
public class VectorAngleOutlierDetection implements OutlierDetection {

    private double maxAngleFromMean;

    public VectorAngleOutlierDetection(double maxAngleFromMean) {
        this.maxAngleFromMean = maxAngleFromMean;
    }

    @Override
    public List<FeaturePointMatch<SurfFeaturePoint>> removeOutliers(List<FeaturePointMatch<SurfFeaturePoint>> input) {
        double[] angles = new double[input.size()];
        for (int i = 0; i < input.size(); i++) {
            SurfFeaturePoint p1 = input.get(i).first;
            SurfFeaturePoint p2 = input.get(i).second;

            angles[i] = Math.toDegrees(Math.atan2(p2.getY() - p1.getY(), p2.getX() - p1.getX()));
        }

        double mean = new Mean().evaluate(angles);

        List<FeaturePointMatch<SurfFeaturePoint>> output = new ArrayList<>();
        for (int i = 0; i < angles.length; i++) {
            if (Math.abs(mean - angles[i]) < maxAngleFromMean) {
                output.add(input.get(i));
            }
        }

        return output;
    }
}
