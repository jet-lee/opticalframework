package de.virality.opticalframework.odometry.pedometer;

/**
 * Common interface for components providing a step count estimation.
 */
public interface Pedometer {

    /**
     * @return the current step count estimation
     */
    int getStepCount();
}
