package de.virality.opticalframework.odometry.gui;

import de.virality.opticalframework.core.gui.Visualization;
import de.virality.opticalframework.core.gui.drawer.Drawer;
import de.virality.opticalframework.odometry.OdometryPath;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.List;

/**
 * A {@link Drawer} to visualize an associated {@link OdometryPath}.
 * <p>
 * Whenever the underlying {@link OdometryPath} is updated, you should call {@link Visualization#refresh()} to reflect
 * the changes.
 */
public class PathDrawer implements Drawer {

    private static final Color DEFAULT_COLOR = Color.BLUE;
    private static final int STROKE_WIDTH = 2;
    private static final int MARKER_SIZE = 8;

    private final Paint paint;
    private final Stroke stroke = new BasicStroke(STROKE_WIDTH);

    private OdometryPath path;

    public PathDrawer(OdometryPath path) {
        this(path, DEFAULT_COLOR);
    }

    public PathDrawer(OdometryPath path, Paint color) {
        this.path = path;
        this.paint = color;
    }

    public void setPath(OdometryPath path) {
        this.path = path;
    }

    @Override
    public void draw(Graphics2D graphics2D, double scaleX, double scaleY) {
        if (path == null || path.getWayPoints().isEmpty()) {
            return;
        }

        graphics2D.setStroke(stroke);
        graphics2D.setPaint(paint);

        List<Point2D> wayPoints = path.getWayPoints();
        int[] xCoords = new int[wayPoints.size()];
        int[] yCoords = new int[wayPoints.size()];

        for (int i = 0; i < wayPoints.size(); i++) {
            Point2D p = wayPoints.get(i);
            xCoords[i] = (int) (p.getX() * scaleX);
            yCoords[i] = (int) (p.getY() * scaleY);
        }

        graphics2D.drawPolyline(xCoords, yCoords, xCoords.length);

        // start point marker
        graphics2D.fillRect(
                xCoords[0] - MARKER_SIZE / 2,
                yCoords[0] - MARKER_SIZE / 2,
                MARKER_SIZE, MARKER_SIZE);

        // current point marker
        graphics2D.fillOval(
                xCoords[xCoords.length - 1] - MARKER_SIZE / 2,
                yCoords[yCoords.length - 1] - MARKER_SIZE / 2,
                MARKER_SIZE, MARKER_SIZE);
    }
}
