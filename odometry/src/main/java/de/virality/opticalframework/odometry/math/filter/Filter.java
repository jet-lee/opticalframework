package de.virality.opticalframework.odometry.math.filter;

/**
 * Common interface for smoothing filters that can be applied to a numerical data stream.
 * <p>
 * It should be kept in mind, since data is processed live and most implementations require some form of a smoothing
 * window, that applying a filter might introduce some kind of 'delay' to the data stream.
 */
public interface Filter {

    /**
     * Filters a new value.
     * @param value A new data point
     * @return A filtered data point. Keep in mind, that due to filtering delays, this does not necessarily have to be
     *         the filtered equivalent to the newly added point, but rather some point in past.
     */
    double filter(double value);
}
