package de.virality.opticalframework.odometry.pedometer;

import de.virality.opticalframework.core.annotations.Experimental;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.util.MathUtils;
import de.virality.opticalframework.odometry.math.filter.Filter;
import de.virality.opticalframework.odometry.math.peak.PeakDetection;

import java.util.List;

/**
 * A {@link Pedometer} detecting steps by analyzing the movement of matched {@link SurfFeaturePoint} pairs throughout
 * frames of a video stream.
 * <p>
 * For each input, the mean of the euclidean distances of the feature point matches are calculated. The resulting data
 * stream is then filtered and maxima are detected and counted as steps.
 * <p>
 * The concept was presented in: <br/>
 * <em>"Visuelle Schritterkennung"</em>, Bachelor thesis by Georg Hagemann (2016) at LMU München, Informatik, Lehrstuhl
 * für Mobile Und Verteilte Systeme
 */
@Experimental("Only basic concept was implemented yet, without tweaks or correction steps as described in the thesis.")
public class SurfMovementPedometer extends BaseSurfPedometer {

    public SurfMovementPedometer(Filter filter, PeakDetection peakDetection) {
        super(filter, peakDetection);
    }

    @Override
    protected double getFrameRawValue(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        return getMeanVectorMovement(matches);
    }

    private double getMeanVectorMovement(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        if (matches == null || matches.isEmpty()) {
            return 0;
        }

        double sum = 0;
        for (FeaturePointMatch<SurfFeaturePoint> match : matches) {
            SurfFeaturePoint p1 = match.first;
            SurfFeaturePoint p2 = match.second;

            double distance = MathUtils.euclideanDistance(new double[]{p1.getX(), p1.getY()},
                    new double[]{p2.getX(), p2.getY()});
            int sign = p1.getY() > p2.getY() ? -1 : 1;
            sum += distance * sign;
        }

        return sum / matches.size();
    }
}
