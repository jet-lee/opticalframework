package de.virality.opticalframework.odometry.pedometer;

import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.util.VideoUtils;
import de.virality.opticalframework.odometry.math.filter.Filter;
import de.virality.opticalframework.odometry.math.filter.SGFilter;
import de.virality.opticalframework.odometry.math.peak.PeakDetection;
import de.virality.opticalframework.odometry.math.peak.WindowPeakDetection;
import org.opencv.highgui.VideoCapture;

import java.util.List;

/**
 * A {@link Pedometer} detecting steps by analyzing changes in the orientation attribute of matched
 * {@link SurfFeaturePoint} pairs throughout frames of a video stream.
 * <p>
 * There are two modes:
 * <p>
 * {@link Mode#SCORE}: For each input, a so called orientation score is calculated. It's values can lie in the range
 * [-1, 1]. In simple words, a value of 1 or -1 says, that all feature point orientations have changed in the same
 * direction, while a value of 0 says, that half of the orientations changed to one direction while the other half
 * changed to the opposite direction.
 * <p>
 * {@link Mode#MEAN}: For each input, the mean of the differences of the orientation attributes of the matched
 * {@link SurfFeaturePoint} pairs is calculated.
 * <p>
 * For both modes, the resulting data stream is then filtered and maxima are detected and counted as steps.
 * <p>
 * Given the video source or corresponding video properties, a {@link SurfOrientationPedometer} instance can
 * automatically be configured by using default parameters for the {@link Filter} and {@link PeakDetection}. See
 * {@link #createDefaultFor(VideoCapture)} and similar methods.
 * <p>
 * The concept was presented in: <br/>
 * <em>"Visuelle Schritt- und Aktivitätserkennung auf Basis des SURF-Algorithmus"</em>, Bachelor thesis by Maximilian
 * Christl (2016) at LMU München, Informatik, Lehrstuhl für Mobile Und Verteilte Systeme
 */
public class SurfOrientationPedometer extends BaseSurfPedometer {

    /**
     * Creates a pedometer instance with default parameters considering the vertical video resolution.
     *
     * @param capture the video capture
     * @return a fully configured {@link SurfOrientationPedometer}
     * @throws IllegalStateException if the vertical resolution of the video could not be determined automatically
     *
     * @see #createDefaultFor(int)
     * @see #createDefaultFor(double)
     * @see #createDefaultFor(VideoUtils.Res16x9)
     */
    public static SurfOrientationPedometer createDefaultFor(VideoCapture capture) {
        int frameHeight = VideoUtils.getVerticalResolution(capture);
        if (frameHeight == 0) {
            throw new IllegalStateException("Frame height is 0! VideoCapture might not be initialized yet.");
        }
        return createDefaultFor(frameHeight);
    }

    /**
     * Creates a pedometer instance with default parameters considering the vertical video resolution.
     *
     * @param videoResolution the video resolution
     * @return a fully configured {@link SurfOrientationPedometer}
     *
     * @see #createDefaultFor(int)
     * @see #createDefaultFor(VideoCapture)
     * @see #createDefaultFor(double)
     */
    public static SurfOrientationPedometer createDefaultFor(VideoUtils.Res16x9 videoResolution) {
        return createDefaultFor(videoResolution.height);
    }

    /**
     * Creates a pedometer instance with default parameters considering the vertical video resolution.
     *
     * @param verticalVideoResolution the vertical video resolution
     * @return a fully configured {@link SurfOrientationPedometer}
     *
     * @see #createDefaultFor(VideoCapture)
     * @see #createDefaultFor(double)
     * @see #createDefaultFor(VideoUtils.Res16x9)
     */
    public static SurfOrientationPedometer createDefaultFor(int verticalVideoResolution) {
        int filterWindowSize;
        int filterDegree;
        double peakThreshold;
        int peakWindowSize;

        if (verticalVideoResolution <= 180) {
            filterWindowSize = 19;
            filterDegree = 7;
            peakThreshold = 0.15;
            peakWindowSize = 8;
        } else if (verticalVideoResolution <= 360) {
            filterWindowSize = 17;
            filterDegree = 6;
            peakThreshold = 0.13;
            peakWindowSize = 8;
        } else if (verticalVideoResolution <= 720) {
            filterWindowSize = 27;
            filterDegree = 10;
            peakThreshold = 0.10;
            peakWindowSize = 7;
        } else {
            filterWindowSize = 25;
            filterDegree = 8;
            peakThreshold = 0.10;
            peakWindowSize = 6;
        }

        return new SurfOrientationPedometer(new SGFilter(filterWindowSize, filterDegree),
                new WindowPeakDetection(peakWindowSize, peakThreshold, true));
    }

    /**
     * Creates a pedometer instance with default parameters considering the vertical video resolution.
     *
     * @param videoFramesPerSecond the frame rate of the video
     * @return a fully configured {@link SurfOrientationPedometer}
     *
     * @see #createDefaultFor(int)
     * @see #createDefaultFor(VideoCapture)
     * @see #createDefaultFor(VideoUtils.Res16x9)
     */

    public static SurfOrientationPedometer createDefaultFor(double videoFramesPerSecond) {
        int filterWindowSize;
        int filterDegree;
        double peakThreshold;
        int peakWindowSize;

        int fps = (int) Math.round(videoFramesPerSecond);
        if (fps <= 10) {
            filterWindowSize = 21;
            filterDegree = 6;
            peakThreshold = 0.17;
            peakWindowSize = 8;
        } else if (fps <= 15) {
            filterWindowSize = 15;
            filterDegree = 5;
            peakThreshold = 0.16;
            peakWindowSize = 8;
        } else {
            filterWindowSize = 15;
            filterDegree = 5;
            peakThreshold = 0.15;
            peakWindowSize = 9;
        }

        return new SurfOrientationPedometer(new SGFilter(filterWindowSize, filterDegree),
                new WindowPeakDetection(peakWindowSize, peakThreshold, true));

    }

    public enum Mode { SCORE, MEAN }

    private final Mode mode;

    public SurfOrientationPedometer(Filter filter, PeakDetection peakDetection) {
        this(filter, peakDetection, Mode.SCORE);
    }

    public SurfOrientationPedometer(Filter filter, PeakDetection peakDetection, Mode mode) {
        super(filter, peakDetection);
        this.mode = mode;
    }

    @Override
    protected double getFrameRawValue(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        return mode == Mode.SCORE ? getOrientationScore(matches) : getMeanOrientationDelta(matches);
    }

    private double getOrientationScore(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        if (matches == null || matches.isEmpty()) {
            return 0;
        }

        double sum = 0;
        for (FeaturePointMatch<SurfFeaturePoint> match : matches) {
            SurfFeaturePoint p1 = match.first;
            SurfFeaturePoint p2 = match.second;

            double delta = p1.getOrientation() - p2.getOrientation();

            if (delta < -Math.PI) {
                delta = 0 - (delta + Math.PI);
            } else if (delta > Math.PI) {
                delta = 0 - (delta - Math.PI);
            }

            sum += Math.signum(delta);
        }

        return sum / matches.size();
    }

    private double getMeanOrientationDelta(List<FeaturePointMatch<SurfFeaturePoint>> matches) {
        if (matches == null || matches.isEmpty()) {
            return 0;
        }

        double sum = 0;
        for (FeaturePointMatch<SurfFeaturePoint> match : matches) {
            SurfFeaturePoint p1 = match.first;
            SurfFeaturePoint p2 = match.second;

            double delta = p1.getOrientation() - p2.getOrientation();

            if (delta < -Math.PI) {
                delta = -delta - Math.PI;
            } else if (delta > Math.PI) {
                delta =  -delta + Math.PI;
            }

            sum += delta;
        }

        return sum / matches.size();
    }
}
