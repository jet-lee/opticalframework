package de.virality.opticalframework.odometry.video;

import de.virality.opticalframework.core.feature.FeaturePointExtractor;
import de.virality.opticalframework.core.feature.Surf;
import de.virality.opticalframework.core.feature.SurfFeaturePoint;
import de.virality.opticalframework.core.matcher.FeaturePointMatch;
import de.virality.opticalframework.core.matcher.FeaturePointMatcher;
import de.virality.opticalframework.core.matcher.ParallelFeaturePointMatcher;
import de.virality.opticalframework.odometry.outlier.OutlierDetection;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * A class that can be used to perform matching of video frames in parallel.
 * <p>
 * Basic usage involves constructing an instance of this class, call {@link #startProcessing()} and subsequently call
 * {@link #next()}, which will block until a new frame was processed. You can call {@link #setResize(int, int)} to
 * change the video resolution on the fly.
 * <p>
 * The number of threads used is determined by the number of available CPU cores as returned by
 * {@code Runtime.getRuntime().availableProcessors()};
 * <p>
 * Usage example:
 * <pre>
 *      ParallelVideoProcessor processor = new ParallelVideoProcessor(...);
 *      processor.startProcessing();
 *
 *      while (processor.next()) {
 *          // Data for new frame is available now
 *          List<FeaturePointMatch<SurfFeaturePoint>> matches = processor.getFramePointMatches();
 *          List<SurfFeaturePoints> surfPoints = processor.getFrameFeaturePoints();
 *          Mat frame = processor.getFrameImage();
 *
 *          ...
 *      }
 * </pre>
 */
public class ParallelVideoProcessor {

    private static final int NUM_THREADS = Runtime.getRuntime().availableProcessors();

    private final TaskProducer taskProducer;

    private final BlockingQueue<Future<FrameResult>> taskQueue = new ArrayBlockingQueue<>(NUM_THREADS, true);

    private FrameResult currentFrame;

    /**
     * @param video The video to process
     * @param surf A surf detector to extract feature points
     * @param outlierDetection An outlier detection to remove false matches. Can be null.
     */
    public ParallelVideoProcessor(VideoCapture video, Surf surf, OutlierDetection outlierDetection) {
        this.taskProducer = new TaskProducer(video, surf, outlierDetection, taskQueue);
    }

    /**
     * Set a desired frame resolution. The video will be resized on the fly.
     *
     * @param width The target width
     * @param height The target height
     */
    public void setResize(int width, int height) {
        taskProducer.setTargetFrameSize(width, height);
    }

    /**
     * Starts the video processing. Subsequently, you can call {@link #next()} to retrieve data.
     */
    public void startProcessing() {
        taskProducer.start();
    }

    /**
     * Moves to the next frame. This will block until data for the next frame is available.
     * <p>
     * After calling this, you can retrieve the new frame data by using {@link #getFrameFeaturePoints()},
     * {@link #getFrameImage()} and {@link #getFramePointMatches()}.
     * You have to call {@link #startProcessing()} first.
     *
     * @return False, if the last frame was reached or an error occured, true otherwise
     */
    public boolean next() {
        if (taskProducer.hasReachedEnd() && taskQueue.isEmpty()) {
            return false;
        }

        try {
            Future<FrameResult> task = taskQueue.take();
            currentFrame = task.get();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Gets the list of {@link SurfFeaturePoint}s detected in the current frame.
     * <p>
     * You have to call {@link #next()} at least once before using this method, otherwise an Exception will be thrown.
     *
     * @return A list of {@link SurfFeaturePoint}s for the current frame
     */
    public List<SurfFeaturePoint> getFrameFeaturePoints() {
        if (currentFrame == null) {
            throw new IllegalStateException("No frame is currently selected! " +
                    "You may have reached the end of the video or you need to call next() first.");
        }
        return currentFrame.featurePoints;
    }

    /**
     * Gets the current frame image.
     * <p>
     * You have to call {@link #next()} at least once before using this method, otherwise an Exception will be thrown.
     *
     * @return The current frame image
     */
    public Mat getFrameImage() {
        if (currentFrame == null) {
            throw new IllegalStateException("No frame is currently selected! " +
                    "You may have reached the end of the video or you need to call next() first.");
        }
        return currentFrame.frame;
    }

    /**
     * Gets the list of point matches of the current frame with the previous frame.
     * <p>
     * You have to call {@link #next()} at least once before using this method, otherwise an Exception will be thrown.
     *
     * @return A list of point matches for the current frame
     */
    public List<FeaturePointMatch<SurfFeaturePoint>> getFramePointMatches() {
        if (currentFrame == null) {
            throw new IllegalStateException("No frame is currently selected! " +
                    "You may have reached the end of the video or you need to call next() first.");
        }
        return currentFrame.matches;
    }

    private static class FrameResult {
        final Mat frame;
        final List<SurfFeaturePoint> featurePoints;
        final List<FeaturePointMatch<SurfFeaturePoint>> matches;

        FrameResult(Mat frame, List<SurfFeaturePoint> featurePoints,
                    List<FeaturePointMatch<SurfFeaturePoint>> matches) {
            this.frame = frame;
            this.featurePoints = featurePoints;
            this.matches = matches;
        }
    }

    private static class TaskProducer extends Thread {

        private final VideoCapture capture;
        private final FeaturePointExtractor<SurfFeaturePoint> extractor;
        private final OutlierDetection outlierDetection;
        private final BlockingQueue<Future<FrameResult>> outputQueue;

        private final ExecutorService executorService = Executors.newFixedThreadPool(NUM_THREADS);
        private final FeaturePointMatcher pointMatcher = new ParallelFeaturePointMatcher();

        private Size targetFrameSize;
        private boolean reachedEnd = false;

        TaskProducer(VideoCapture capture, FeaturePointExtractor<SurfFeaturePoint> extractor,
                     OutlierDetection outlierDetection, BlockingQueue<Future<FrameResult>> outputQueue) {
            this.capture = capture;
            this.extractor = extractor;
            this.outlierDetection = outlierDetection;
            this.outputQueue = outputQueue;
        }

        void setTargetFrameSize(int width, int height) {
            targetFrameSize = new Size(width, height);
        }

        synchronized boolean hasReachedEnd() {
            return reachedEnd;
        }

        @Override
        public void run() {
            Future<FrameResult> previousFuture = null;

            while (hasNextFrame()) {
                Mat frame = new Mat();
                capture.retrieve(frame);

                if (targetFrameSize != null) {
                    frame = resizeFrame(frame);
                }

                final Future<FrameResult> finalPreviousFuture = previousFuture;
                final Mat finalFrame = frame;
                Future<FrameResult> future = executorService.submit(() -> processFrame(finalFrame, finalPreviousFuture));

                try {
                    outputQueue.put(future);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }

                previousFuture = future;
            }

            executorService.shutdown();
        }

        private synchronized boolean hasNextFrame() {
            reachedEnd = !capture.grab();
            return !reachedEnd;
        }

        private Mat resizeFrame(Mat src) {
            Mat target = new Mat();
            Imgproc.resize(src, target, targetFrameSize);
            return target;
        }

        private FrameResult processFrame(Mat frame, Future<FrameResult> previousFrame)
                throws ExecutionException, InterruptedException {
            List<SurfFeaturePoint> points = extractor.extractFeaturePoints(frame);

            List<FeaturePointMatch<SurfFeaturePoint>> matches;
            if (previousFrame != null) {
                FrameResult previousFrameResult = previousFrame.get();
                matches = pointMatcher.findMatches(previousFrameResult.featurePoints, points);
                if (outlierDetection != null) {
                    matches = outlierDetection.removeOutliers(matches);
                }
            } else {
                matches = new ArrayList<>();
            }

            return new FrameResult(frame, points, matches);
        }
    }
}
