package de.virality.opticalframework.odometry.gui;

import de.virality.opticalframework.core.gui.drawer.Drawer;

import java.awt.*;

/**
 * A {@link Drawer} that shows the current step count and heading information.
 */
public class OdometryDrawer implements Drawer {

    private final static int FONT_SIZE = 12;

    private final static int PADDING = 10;

    private final Stroke stroke = new BasicStroke(2);

    private final Paint textPaint = Color.YELLOW;

    private final Paint backgroundPaint = new Color(0, 0, 0, 150);

    private final Font font = new Font("sans-serif", Font.BOLD, FONT_SIZE);

    private int stepCount = 0;

    private int heading = 0;

    public void setStepCount(int stepCount) {
        this.stepCount = stepCount;
    }

    public void setHeading(int heading) {
        this.heading = heading;
    }

    @Override
    public void draw(Graphics2D graphics2D, double scaleX, double scaleY) {
        graphics2D.setPaint(backgroundPaint);
        graphics2D.fillRect(0, 0, FONT_SIZE * 4, 2 * FONT_SIZE + 3 * PADDING);

        graphics2D.setStroke(stroke);
        graphics2D.setPaint(textPaint);
        graphics2D.setFont(font);

        graphics2D.drawString(String.valueOf(stepCount), PADDING, PADDING + FONT_SIZE);
        graphics2D.drawString(String.valueOf(heading), PADDING, 2 * (PADDING + FONT_SIZE));
    }
}
