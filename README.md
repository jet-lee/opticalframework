# Optical Framework

This project consists of several components:
 * The core library (`/core`)
 * A module for image matching (`/image-matching`)
 * A module for visual odometry (`/odometry`)
 * A collection of sample projects (`/samples`).


## Building

[Gradle](http://gradle.org/) is used for building.

### Prerequisites

 * To build the library, [OpenCV](http://opencv.org/) needs to be installed. This project was tested to work with
   OpenCV v2.4.12. Other versions might work as well, but OpenCV 3 is not supported. Check out the OpenCV documentation
   on how to install it or use a package manager available for your system (Mac/Linux).

 * You also need the native bindings for OpenCV-Java (matching your OpenCV version, e.g. v2.4.12). If you can't find a
   prebuilt binary, you can build them yourself.
   [This tutorial](http://docs.opencv.org/2.4/doc/tutorials/introduction/desktop_java/java_dev_intro.html) might be
   helpful (but make sure to build the correct version!). Copy the binary named `libopencv_java2412.so`
   (on Mac: `libopencv_java2412.dylib`, on Windows: `opencv_java2412.dll`) to `/core/nativeLibs/`.

### Building the core library

 * You can now build the core library by running:

   `./gradlew core:build`

 * If building fails, you might have to adjust some variables in the build script to match your system configuration:
   Open `/core/build.gradle` and adjust the lines with definitions for `javaHome` and `openCvInclude`.

   - `echo $JAVA_HOME` usually gives you your Java Home

   - `pkg-config --cflags opencv` helps you finding the include path for OpenCV

 * The build outputs a `.jar`-File to `/core/build/libs`.
   It also produces a native shared library, that can be found in `/core/build/binaries/opensurfSharedLibrary`. It is
   named `libopensurf.so` (Linux), `libopensurf.dylib` (Mac) or `opensurf.dll` (Windows) and contains the native
   bindings to the OpenSURF C++ library.

 * If you require one of the additional modules, you can build them similarly by running:

    `./gradlew odometry:build` or `./gradlew image-matching:build`


## Running samples

If you want to run one of the sample projects, make sure to first build and copy all native binaries (`libopencv_java`
and `libopensurf`) to `/samples/<name of the sample project>/nativeLibs/`. You can then run the sample:

`./gradlew samples:<name of the sample project>:run`


## How to include the library in your project

1. Build the core library and required modules as explainedg above.
2. Include the produced `.jar` file(s) as dependency to your project.
3. Make sure the `libopencv_java` and `libopensurf` binaries are included in your `java_library_path`. Either copy them
   to there or change the `java_library_path`, so Java can find them (e.g. Gradle lets you do this, check out the
   `gradle.build` file from one of the sample projects).


## Usage

You always have to load the native libraries first:
```
static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME); // loads the OpenCV bindings
    System.loadLibrary(Surf.NATIVE_LIBRARY_NAME); // loads the OpenSURF bindings
}
```

A simple example, that extracts SURF feature points from an image and displays them:
```
static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    System.loadLibrary(Surf.NATIVE_LIBRARY_NAME);
}

(...)

Surf surf = new Surf();
Mat img = ImageUtils.loadImageFromPath("/path/to/image/example.jpg");
List<SurfFeaturePoint> featurePoints = surf.extractFeaturePoints(img);

ImageVisualization visualization = new ImageVisualization(img);
visualization.addDrawer(new FeaturePointDrawer<>(featurePoints, FeaturePointDrawer.Style.SURF));
visualization.show();
```

Take a look at the sample projects for more detailed examples.
